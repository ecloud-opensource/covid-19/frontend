// Modules
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import { NgSelectModule } from '@ng-select/ng-select';
import { TranslateModule } from '@ngx-translate/core';
// Components
import { FooterComponent } from './components/footer/footer.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { PaginationComponent } from './components/pagination/pagination.component';
import { InputFileComponent } from './components/input-file/input-file.component';
import { InputDateTimeComponent } from './components/input-date-time/input-date-time.component';
import { SelectSearchComponent } from './components/select-search/select-search.component';
import { CardStatisticsComponent } from './components/card-statistics/card-statistics.component';
import { LogComponent } from './components/log/log.component';
import { ModalAddCaseComponent } from './components/modal-add-case/modal-add-case.component';
import { ModalRemoveCaseComponent } from './components/modal-remove-case/modal-remove-case.component';

const importExport = [
  NgbModule,
  CommonModule,
  RouterModule,
  FormsModule,
  ReactiveFormsModule,
  HttpClientModule,
  NgSelectModule,
  TranslateModule
];

const declarationExport = [
  FooterComponent,
  SidebarComponent,
  NavbarComponent,
  PaginationComponent,
  InputFileComponent,
  InputDateTimeComponent,
  SelectSearchComponent,
  CardStatisticsComponent,
  LogComponent
];

@NgModule({
  declarations: [
    ...declarationExport,
    ModalAddCaseComponent,
    ModalRemoveCaseComponent
  ],
  imports: [
    SweetAlert2Module.forRoot(),
    ...importExport
  ],
  exports: [
    SweetAlert2Module,
    ...declarationExport,
    ...importExport
  ]
})
export class SharedModule { }
