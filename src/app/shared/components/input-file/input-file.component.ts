import { Component, OnInit, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { UploadService } from 'src/app/core/services/upload/upload.service';
import { EndpointsConstants } from 'src/app/helper/endpoints';
import { CatalogUploadTypeClass } from 'src/app/helper/catalog';

@Component({
  selector: 'app-input-file',
  templateUrl: './input-file.component.html',
  styleUrls: ['./input-file.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class InputFileComponent implements OnInit {
  fileName: string;

  @Input() endpoint = EndpointsConstants.upload.image;
  @Input() icon = 'ft-upload';
  @Input() buttonText = 'forms.buttons.uploadFile';
  @Input() showPreviewImage = true;
  @Input() previewUrl: string | ArrayBuffer;
  @Input() allowedMimeTypes = 'image/*';
  @Input() disabled = false;

  @Output() sendFile = new EventEmitter();

  constructor(private uploadService: UploadService) { }

  ngOnInit() {
  }

  openFileBrowser(event: any) {
    event.preventDefault();
    const element: HTMLElement = document.getElementById('uploadFile') as HTMLElement;
    element.click();
  }

  onFileChange(event) {
    const file = event.target.files[0];
    this.fileName = file.name;
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = _ => {
      this.previewUrl = reader.result;
      this.uploadImage(file);
    };
  }

  uploadImage(file) {
    this.uploadService.uploadFiles(file, this.endpoint, CatalogUploadTypeClass.avatar).subscribe(
      response => {
        this.sendFile.emit(response.data['path']);
      }
    );
  }
}
