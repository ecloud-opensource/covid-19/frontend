import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';
import { Log } from 'src/app/models/log';

@Component({
  selector: 'app-log',
  templateUrl: './log.component.html',
  styleUrls: ['./log.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LogComponent implements OnInit {
  @Input() title: string;
  @Input() logs: Log[];

  constructor() { }

  ngOnInit() {
  }

}
