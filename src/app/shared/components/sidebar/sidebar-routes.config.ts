import { RouteInfo } from './sidebar.metadata';

export const ROUTES: RouteInfo[] = [
    {
        path: '/dashboard', title: 'modules.shared.sidebar.dashboard', icon: 'fas fa-chart-line', class: '', badge: '', badgeClass: 'badge badge-pill badge-danger float-right mr-1 mt-1', isExternalLink: false, submenu: []
    },
    {
        path: '/colaborator/dashboard', title: 'modules.shared.sidebar.colaborator', icon: 'fas fa-user', class: '', badge: '', badgeClass: 'badge badge-pill badge-danger float-right mr-1 mt-1', isExternalLink: false, submenu: []
    },
    {
        path: '', title: 'modules.shared.sidebar.catalog.base', icon: 'far fa-file-alt', class: 'has-sub', badge: '', badgeClass: 'badge badge-pill badge-danger float-right mr-1 mt-1', isExternalLink: false,
        submenu: [
            {
                path: '/admin/list', title: 'modules.shared.sidebar.catalog.administrators', icon: '', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: []
            },
            {
                path: '/country/list', title: 'modules.shared.sidebar.catalog.countries', icon: '', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: []
            },
            {
                path: '/state/list', title: 'modules.shared.sidebar.catalog.states', icon: '', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: []
            },
            {
                path: '/city/list', title: 'modules.shared.sidebar.catalog.cities', icon: '', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: []
            },
            {
                path: '/neighborhood/list', title: 'modules.shared.sidebar.catalog.neighborhoods', icon: '', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: []
            },
            {
                path: '/link/list', title: 'modules.shared.sidebar.catalog.linksOfInterest', icon: '', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: []
            }
        ]
    },
    {
        // tslint:disable-next-line:max-line-length
        path: '/misbehavior/dashboard', title: 'modules.shared.sidebar.misbehaviorRecord', icon: 'fas fa-exclamation-circle', class: '', badge: '', badgeClass: 'badge badge-pill badge-danger float-right mr-1 mt-1', isExternalLink: false, submenu: []
    },
    {
        path: '/case-record/dashboard', title: 'modules.shared.sidebar.caseRecord', icon: 'fas fa-clipboard', class: '', badge: '', badgeClass: 'badge badge-pill badge-danger float-right mr-1 mt-1', isExternalLink: false, submenu: []
    },
    {
        path: '/city-feed/list', title: 'modules.shared.sidebar.cityFedd', icon: 'far fa-newspaper', class: '', badge: '', badgeClass: 'badge badge-pill badge-danger float-right mr-1 mt-1', isExternalLink: false, submenu: []
    },
    {
        path: '/settings/edit', title: 'modules.shared.sidebar.settings', icon: 'fas fa-cogs', class: '', badge: '', badgeClass: 'badge badge-pill badge-danger float-right mr-1 mt-1', isExternalLink: false, submenu: []
    }
];
