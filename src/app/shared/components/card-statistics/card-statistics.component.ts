import { Component, OnInit, ChangeDetectionStrategy, Input, ChangeDetectorRef } from '@angular/core';
import { UserService } from 'src/app/core/services/user/user.service';
import { Colaborator } from 'src/app/models/colaborator';
import { getClassByStatus } from 'src/app/helper/utils';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-card-statistics',
  templateUrl: './card-statistics.component.html',
  styleUrls: ['./card-statistics.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CardStatisticsComponent implements OnInit {
  colaborator: Colaborator;
  status: string;
  id: number;

  @Input() title: string;
  @Input() subtitle: string;
  @Input() classTitle: string;
  @Input() classSubTitle: string;
  @Input() statusClass: string;

  constructor(
    private userServices: UserService,
    private cdr: ChangeDetectorRef,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.id = +this.route.snapshot.paramMap.get('id');
    // this.getProfile();
  }

  getProfile() {
    this.userServices.getOne<Colaborator>(this.id).subscribe( response => {
      this.colaborator = new Colaborator(response.data);
      this.statusClass = getClassByStatus(this.colaborator.status);
      this.cdr.detectChanges();
    });
  }


}
