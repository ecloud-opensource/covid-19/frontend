import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CountryService } from 'src/app/core/services/country/country.service';
import { CityService } from 'src/app/core/services/city/city.service';
import { NeighborhoodService } from 'src/app/core/services/neighborhood/neighborhood.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Country } from 'src/app/models/country';
import { City } from 'src/app/models/city';
import { Neighborhood } from 'src/app/models/neighborhood';
import { BaseResponse } from 'src/app/models/base-response';
import { EndpointsConstants } from 'src/app/helper/endpoints';
import { isUndefined, isNullOrEmptyOrUndefined } from 'src/app/helper/utils';
import { ToastService } from 'src/app/core/services/toast/toast.service';
import { State } from 'src/app/models/state';
import { StateService } from 'src/app/core/services/state/state.service';

@Component({
  selector: 'app-modal-add-case',
  templateUrl: './modal-add-case.component.html',
  styleUrls: ['./modal-add-case.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ModalAddCaseComponent implements OnInit {
  caseForm: FormGroup;
  countries: Country[] = [];
  states: State[] = [];
  cities: City[] = [];
  neighborhoods: Neighborhood[] = [];

  constructor(
    public activeModal: NgbActiveModal,
    private cdr: ChangeDetectorRef,
    private fb: FormBuilder,
    private toast: ToastService,
    private countryService: CountryService,
    private stateService: StateService,
    private cityService: CityService,
    private neighborhoodService: NeighborhoodService
  ) {
    this.caseForm = this.fb.group({
      idCountry: [null, Validators.required],
      idState: [null],
      idCity: [null],
      idNeighborhood: [null],
      quantity: [1, [Validators.required, Validators.min(1)]]
    });
  }

  ngOnInit() {
    this.getCountries();
  }

  getCountries(term?) {
    const filters = {
      term: term ? term : ''
    };
    this.countryService.get<Country>(1, EndpointsConstants.requestLimit, filters).subscribe(response => {
      this.countries = new BaseResponse<Country>(response, Country).data['rows'];
      this.cdr.detectChanges();
    });
  }

  onCountryChange(country) {
    if (!isUndefined(country)) {
      this.getStates(country.id);
      this.getCities(country.id);
    } else {
      this.states = [];
      this.cities = [];
    }
    this.caseForm.get('idState').setValue(null);
    this.caseForm.get('idCity').setValue(null);
    this.caseForm.get('idNeighborhood').setValue(null);
    this.cdr.detectChanges();
  }

  onStateChange(state) {
    if (!isUndefined(state)) {
      this.getCities(this.caseForm.get('idCountry').value, state.id);
    } else {
      this.cities = [];
    }
    this.caseForm.get('idCity').setValue(null);
    this.caseForm.get('idNeighborhood').setValue(null);
    this.cdr.detectChanges();
  }

  getStates(idCountry, term?) {
    const filters = {
      idCountry,
      term: term ? term : ''
    };
    this.stateService.get<State>(1, EndpointsConstants.requestLimit, filters).subscribe(response => {
      this.states = new BaseResponse<State>(response, State).data['rows'];
      this.cdr.detectChanges();
    });
  }

  getCities(idCountry, idState?, term?) {
    const filters = {
      idCountry,
      term: term ? term : ''
    };
    if (!isNullOrEmptyOrUndefined(idState)) {
      filters['idState'] = idState;
    }
    this.cityService.get<City>(1, EndpointsConstants.requestLimit, filters).subscribe(response => {
      this.cities = new BaseResponse<City>(response, City).data['rows'];
      this.cdr.detectChanges();
    });
  }

  onCityChange(city) {
    if (!isUndefined(city)) {
      this.getNeighborhoods(city.id);
    } else {
      this.neighborhoods = [];
    }
    this.caseForm.get('idNeighborhood').setValue(null);
    this.cdr.detectChanges();
  }

  getNeighborhoods(idCity, term?) {
    const filters = {
      idCity,
      term: term ? term : ''
    };
    this.neighborhoodService.get<Neighborhood>(1, EndpointsConstants.requestLimit, filters).subscribe(response => {
      this.neighborhoods = new BaseResponse<Neighborhood>(response, Neighborhood).data['rows'];
      this.cdr.detectChanges();
    });
  }

  onSubmit() {
    const formModel = this.caseForm.value;
    if (!isNullOrEmptyOrUndefined(formModel.idNeighborhood)) {
      this.updateNeighborhood();
    } else if (!isNullOrEmptyOrUndefined(formModel.idCity)) {
      this.updateCity();
    } else if (!isNullOrEmptyOrUndefined(formModel.idState)) {
      this.updateState();
    } else {
      this.updateCountry();
    }
  }

  updateCountry() {
    const idCountry = this.caseForm.value.idCountry;
    this.countryService.getOne<Country>(idCountry).subscribe( response => {
      const quantity = +this.caseForm.value.quantity;
      const areInfected = +response.data['areInfected'] + quantity;
      const params = {
        areInfected
      };
      this.countryService.edit<Country>(params, idCountry).subscribe( _ => {
        if (isNullOrEmptyOrUndefined(this.caseForm.value.idNeighborhood)) {
          this.toast.success('modules.shared.modalAddCase.added');
          this.activeModal.dismiss();
        }
      });
    });
  }

  updateState() {
    const idState = this.caseForm.value.idState;
    this.stateService.getOne<State>(idState).subscribe( response => {
      const quantity = +this.caseForm.value.quantity;
      const areInfected = +response.data['areInfected'] + quantity;
      const params = {
        areInfected
      };
      this.stateService.edit<State>(params, idState).subscribe( _ => {
        if (isNullOrEmptyOrUndefined(this.caseForm.value.idNeighborhood)) {
          this.toast.success('modules.shared.modalAddCase.added');
          this.activeModal.dismiss();
        }
      });
    });
  }

  updateCity() {
    const idCity = this.caseForm.value.idCity;
    this.cityService.getOne<City>(idCity).subscribe( response => {
      const quantity = +this.caseForm.value.quantity;
      const areInfected = +response.data['areInfected'] + quantity;
      const params = {
        areInfected
      };
      this.cityService.edit<City>(params, idCity).subscribe( _ => {
        if (isNullOrEmptyOrUndefined(this.caseForm.value.idNeighborhood)) {
          this.toast.success('modules.shared.modalAddCase.added');
          this.activeModal.dismiss();
        }
      });
    });
  }

  updateNeighborhood() {
    const idNeighborhood = this.caseForm.value.idNeighborhood;
    this.neighborhoodService.getOne<Neighborhood>(idNeighborhood).subscribe( response => {
      const quantity = +this.caseForm.value.quantity;
      const areInfected = +response.data['areInfected'] + quantity;
      const params = {
        areInfected
      };
      this.neighborhoodService.edit<Neighborhood>(params, idNeighborhood).subscribe( _ => {
        this.toast.success('modules.shared.modalAddCase.added');
        this.activeModal.dismiss();
      });
    });
  }

}
