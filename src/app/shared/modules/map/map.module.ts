import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AgmCoreModule, MarkerManager } from '@agm/core';
import { AgmJsMarkerClustererModule, ClusterManager } from '@agm/js-marker-clusterer';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';
import { UiSwitchModule } from 'ngx-ui-switch';
import { MapComponent } from './components/map/map.component';
import { MapFilterComponent } from './components/filter/filter.component';
import { MapLoaderComponent } from './components/loader/loader.component';
import { SharedModule } from '../../shared.module';

@NgModule({
  declarations: [
    MapComponent,
    MapFilterComponent,
    MapLoaderComponent
  ],
  imports: [
    CommonModule,
    AgmCoreModule,
    AgmJsMarkerClustererModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    NgSelectModule,
    SharedModule,
    UiSwitchModule
  ],
  exports: [
    AgmCoreModule,
    MapComponent,
    MapFilterComponent,
    MapLoaderComponent
  ],
  providers: [
    MarkerManager,
    ClusterManager
  ]
})
export class MapModule { }
