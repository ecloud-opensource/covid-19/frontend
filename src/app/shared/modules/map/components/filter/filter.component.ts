import { Component, OnInit, OnChanges, OnDestroy, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { Subscription } from 'rxjs';
import { MapFilter } from 'src/app/models/map-filter';
import { CatalogResponsiveBreakpoints } from 'src/app/helper/catalog';

@Component({
  selector: 'app-map-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MapFilterComponent implements OnInit, OnChanges, OnDestroy {
  showFilter = true;
  filterForm: FormGroup;

  private formSubscription: Subscription;

  @Input() filters: MapFilter[] = [];
  @Output() changeFilterForm = new EventEmitter();

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.collapseFilterByScreenSize(CatalogResponsiveBreakpoints.tablet);
    this.initializeForm();
  }

  ngOnChanges() {
    this.initializeForm();
  }

  initializeForm() {
    this.filterForm = this.fb.group({});
    this.toFormGroup(this.filters);
    this.onFilterChange();
  }

  collapseFilterByScreenSize(size: number) {
    if (window.innerWidth <= size) {
      this.showFilter = false;
    }
  }

  toFormGroup(filters: MapFilter[]) {
    filters.forEach(element => {
      this.filterForm.addControl(
        element.key,
        new FormControl(element.value)
      );
    });
  }

  onFilterChange() {
    this.formSubscription = this.filterForm.valueChanges.subscribe( form => {
      this.changeFilterForm.emit(form);
    });
  }

  ngOnDestroy() {
    if (this.formSubscription) { this.formSubscription.unsubscribe(); }
  }

}
