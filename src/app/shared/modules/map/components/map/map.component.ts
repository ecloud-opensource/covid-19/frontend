/// <reference types="@types/googlemaps" />
import { Component, OnInit, ChangeDetectionStrategy, Input, OnChanges, Output, EventEmitter, SimpleChanges, ChangeDetectorRef } from '@angular/core';
import { Point } from 'src/app/models/point';
import { MapFilter } from 'src/app/models/map-filter';
import { isNullOrEmptyOrUndefined } from 'src/app/helper/utils';
import { MapMinZoom } from 'src/app/helper/config';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MapComponent implements OnInit, OnChanges {
  map;
  mapHeatmap;
  showHeatmap = false;
  blockEventBoundsChange = false;

  @Input() showLoader = false;
  @Input() latitude: number;
  @Input() longitude: number;
  @Input() zoom: number;
  @Input() width: string;
  @Input() height: string;
  @Input() routes;
  @Input() markers;
  @Input() polygons;
  @Input() heatmap;
  @Input() filters: MapFilter[] = [];
  @Input() minZoom = MapMinZoom;

  @Output() boundsChange = new EventEmitter();
  @Output() changeFilter = new EventEmitter();

  constructor(
    private cdr: ChangeDetectorRef
  ) { }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.heatmap) {
      this.setHeatmap();
    }
  }

  onMapReady(map) {
    this.map = map;
    this.addListeners();
  }

  setHeatmap() {
    if (this.heatmap.length) {
      // Clear heatmap
      if (!isNullOrEmptyOrUndefined(this.mapHeatmap)) { this.mapHeatmap.setMap(null); }
      this.mapHeatmap = new google.maps.visualization.HeatmapLayer({
        data: this.heatmap,
        radius: 20
      });
      this.mapHeatmap.setMap(this.map);
      this.showHeatmap = true;
    } else if (!isNullOrEmptyOrUndefined(this.mapHeatmap)) {
      this.mapHeatmap.setMap(null);
      this.showHeatmap = false;
    }
  }

  onChangeFilter(event) {
    this.changeFilter.emit(event);
  }

  addListeners() {
    // Drag Start
    this.map.addListener('dragstart', _ => {
      this.blockEventBoundsChange = true;
    });
    // Drag End
    this.map.addListener('dragend', _ => {
      this.blockEventBoundsChange = false;
    });
    // Bounds Change
    this.map.addListener('bounds_changed', _ => {
      if (!this.blockEventBoundsChange) {
        const bounds = this.map.getBounds();
        this.boundsChange.emit({
          topLeft: new Point({ coordinates: [bounds.getSouthWest().lat(), bounds.getNorthEast().lng()] }),
          topRight: new Point({ coordinates: [bounds.getNorthEast().lat(), bounds.getNorthEast().lng()] }),
          bottomLeft: new Point({ coordinates: [bounds.getSouthWest().lat(), bounds.getSouthWest().lng()] }),
          bottomRight: new Point({ coordinates: [bounds.getNorthEast().lat(), bounds.getSouthWest().lng()] })
        });
      }
      this.cdr.detectChanges();
    });
  }
}
