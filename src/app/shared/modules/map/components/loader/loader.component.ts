import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-map-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MapLoaderComponent implements OnInit {
  @Input() width: string;
  @Input() height: string;

  constructor() { }

  ngOnInit() {
  }

}
