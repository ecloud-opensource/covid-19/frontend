import { Validators } from '@angular/forms';

export class FormGroupFilters {
  name: string;
  type: 'input' | 'input-date' |'input-date-range' | 'select' | 'select-search-multiple' | 'select-search-simple';
  options?: any[];
  placeholder?: string;
  valueBind?: string;
  //  Optionals. Send if the filter is type select-search-multiple or select-search-simple.
  labelBind?: string;
  defaultValue?: any;
  validators?: Validators[];
}
