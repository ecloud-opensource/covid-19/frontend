export class ColumnSetting {
    primaryKey: string;
    header?: string;
    format?: string;
    actions?: string[];
    hasLink ?= false;
    link ?= '';
    linkKey?: string;
    hasSort ?= false;
    sortAsc ?= false;
    sortKey?: string;
    isActive ?= false;
    hasBadge ?= false;
    badgeKey?: string;
    badgeValues?: {
        red?: string[];
        green?: string[];
        orange?: string[];
        blue?: string[];
        grey?: string[];
        greySecondary?: string[];
        cyan?: string[];
        yellow?: string[];
    };
    hasColor ?= false;
    colorKey?: string;
    colorValues?: {
        red?: string[];
    };
    isDate ?= false;
    dateFormat?: string;
}
