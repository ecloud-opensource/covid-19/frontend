import { Component, OnInit, Input, OnChanges, EventEmitter, Output, ChangeDetectionStrategy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ColumnSetting } from '../../models/table-layout.model';
import { isUndefined } from 'util';

@Component({
  selector: 'app-table-layout',
  templateUrl: './table-layout.component.html',
  styleUrls: ['./table-layout.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TableLayoutComponent implements OnInit, OnChanges {
  @Input() records: any[];
  @Input() columnSettings: ColumnSetting[];
  @Input() tableActions;
  @Input() routeTitle: string;
  @Input() parent: string;
  @Input() showButtonAdd = true;
  @Input() buttonText: string;
  @Input() buttonIcon: string;
  @Input() perPage: number;
  @Input() totalRecords: number;
  @Input() pageNumber: number;
  @Input() pagination = true;
  @Input() showTitle = true;
  @Input() tableTitle: string;

  @Output() clickDelete = new EventEmitter();
  @Output() clickView = new EventEmitter();
  @Output() clickExport = new EventEmitter();
  @Output() clickEdit = new EventEmitter();
  @Output() changePage = new EventEmitter();
  @Output() changeOrder = new EventEmitter();

  columnMaps: ColumnSetting[];
  keys: string[];
  totalColumns: number;
  editAction = true;
  exportAction = true;
  deleteAction = true;
  viewAction = true;

  constructor(
    private route: ActivatedRoute,
    private router: Router
  ) {
  }

  ngOnInit() {
    this.routeTitle = this.route.snapshot.data.title;
    this.parent = this.route.snapshot.data.parent;
    this.totalColumns = this.columnSettings.length;
    if (this.tableActions) { this.totalColumns++; }
  }

  ngOnChanges() {
    if (this.columnSettings) {
      // Settings provided
      this.columnMaps = this.columnSettings;
    } else {
      // No settings. Use first column
      this.columnMaps = Object.keys(this.records[0])
        .map(key => {
          return {
            primaryKey: key,
            header: key.slice(0, 1).toUpperCase() +
              key.replace(/_/g, ' ').slice(1)
          };
        });
    }
    this.validateActions();
  }

  validateActions() {
    if (this.tableActions) {
      this.exportAction = this.tableActions.some(elem => elem === 'export');
      this.editAction = this.tableActions.some(elem => elem === 'edit');
      this.deleteAction = this.tableActions.some(elem => elem === 'delete');
      this.viewAction = this.tableActions.some(elem => elem === 'view');
    }
  }

  addRoute() {
    this.router.navigate([this.parent, 'add']);
  }

  edit(id) {
    this.clickEdit.emit(id);
  }

  delete(id) {
    this.clickDelete.emit(id);
  }

  view(id) {
    this.clickView.emit(id);
  }

  export(id) {
    this.clickExport.emit(id);
  }

  getColumnInfo(record, primaryKey) {
    let word = '';
    let arrayPrimaryKeys = primaryKey.split(' ').join('');
    arrayPrimaryKeys = arrayPrimaryKeys.split(',');
    arrayPrimaryKeys.forEach(element => {
      const response = this.getData(record, element);
      if (word === '-' || word === '') {
        word = '';
        word = `${word}` + `${(response === null || response === undefined) ? '-' : response}`;
      } else {
        word = word;
      }
    });
    return word.toString();
  }

  getData(record, data) {
    return data.split('.').reduce( (prev, curr) => {
      return prev ? prev[curr] : null;
    }, record || self);
  }

  onChangePage(event) {
    this.changePage.emit(event);
  }

  emitChangeOrder(key) {
    const column = this.columnSettings.find(item => item.sortKey === key);
    if (column !== undefined) {
      this.restartOrder();
      column.sortAsc = !column.sortAsc;
      column.isActive = true;
      this.changeOrder.emit();
    }
  }

  restartOrder() {
    this.columnSettings.map( column => {
      column.isActive = false;
      return column;
    });
  }

  hasBadgeClass(column, item, color) {
    // tslint:disable-next-line:max-line-length
    return (isUndefined(column.badgeValues[color])) ? false : (column.badgeValues[color]).indexOf(this.getColumnInfo(item, column.badgeKey)) > -1 ? true : false;
  }
}
