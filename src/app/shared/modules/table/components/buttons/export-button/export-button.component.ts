import { Component, OnInit, Input, EventEmitter, Output, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-export-action-button',
  templateUrl: './export-button.component.html',
  styleUrls: ['./export-button.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ExportButtonTableComponent implements OnInit {
  @Input() idEntity: string;

  @Output() exportEvent = new EventEmitter();

  constructor() { }

  ngOnInit() { }

  exportAction() {
    this.exportEvent.emit(this.idEntity);
  }
}
