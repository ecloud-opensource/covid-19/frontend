import { Component, OnInit, Input, EventEmitter, Output, ChangeDetectionStrategy } from '@angular/core';

@Component({
    selector: 'app-edit-action-button',
    templateUrl: './edit-button.component.html',
    styleUrls: ['./edit-button.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditButtonTableComponent implements OnInit {
    @Input() idEntity: string;

    @Output() editEvent = new EventEmitter();

    constructor() { }

    ngOnInit() {
    }

    editRoute() {
        this.editEvent.emit(this.idEntity);
    }


}
