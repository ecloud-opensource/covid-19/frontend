import { Component, OnInit, Input, OnChanges, ChangeDetectionStrategy } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormGroupFilters } from '../../../models/table-filter.model';

@Component({
  selector: 'app-select-search-multiple',
  templateUrl: './select-search-multiple.component.html',
  styleUrls: ['./select-search-multiple.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SelectSearchMultipleComponent implements OnInit, OnChanges {
  @Input() filter: FormGroupFilters;
  @Input() form: FormGroup;
  list: any[];

  constructor() { }

  ngOnInit() {
  }

  ngOnChanges() {
    if (this.filter.type === 'select-search-multiple') {
      this.generateList();
    }
  }

  generateList() {
    this.filter.options.forEach(element => {
      const nameComplete = element.name;
      element.nameComplete = nameComplete;
    });
    this.list = this.filter.options;
  }

}
