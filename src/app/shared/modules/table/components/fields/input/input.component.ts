import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';
import { FormGroupFilters } from '../../../models/table-filter.model';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class InputComponent implements OnInit {
  @Input() filter: FormGroupFilters;
  @Input() form: FormGroup;

  constructor() { }

  ngOnInit() {
  }

}
