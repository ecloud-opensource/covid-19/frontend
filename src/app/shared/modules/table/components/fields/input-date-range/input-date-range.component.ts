import { Component, ViewChild, OnInit, ElementRef, Renderer2, Input, EventEmitter, Output, HostListener, ChangeDetectionStrategy } from '@angular/core';
import { NgbDatepicker, NgbInputDatepicker, NgbDateStruct, NgbCalendar, NgbDateAdapter, NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { NgModel, FormGroup } from '@angular/forms';
import { FormGroupFilters } from '../../../models/table-filter.model';
import { isNullOrEmptyOrUndefined } from 'src/app/helper/utils';


const now = new Date();
const equals = (one: NgbDateStruct, two: NgbDateStruct) =>
  one && two && two.year === one.year && two.month === one.month && two.day === one.day;

const before = (one: NgbDateStruct, two: NgbDateStruct) =>
  !one || !two ? false : one.year === two.year ? one.month === two.month ? one.day === two.day
    ? false : one.day < two.day : one.month < two.month : one.year < two.year;

const after = (one: NgbDateStruct, two: NgbDateStruct) =>
  !one || !two ? false : one.year === two.year ? one.month === two.month ? one.day === two.day
    ? false : one.day > two.day : one.month > two.month : one.year > two.year;

@Component({
  selector: 'app-input-date-range',
  templateUrl: './input-date-range.component.html',
  styleUrls: ['./input-date-range.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class InputDateRangeComponent implements OnInit {
  @Input() filter: FormGroupFilters;
  @Input() form: FormGroup;
  @Output() range = new EventEmitter();
  startDate: NgbDateStruct;
  maxDate: NgbDateStruct;
  minDate: NgbDateStruct;
  hoveredDate: NgbDateStruct;
  fromDate: any;
  toDate: any;
  model: any;
  @ViewChild('d', { static: false }) input: NgbInputDatepicker;
  @ViewChild(NgModel, { static: false }) datePick: NgModel;
  @ViewChild('myRangeInput', { static: false }) myRangeInput: ElementRef;

  isHovered = date => this.fromDate && !this.toDate && this.hoveredDate && after(date, this.fromDate) && before(date, this.hoveredDate);
  isInside = date => after(date, this.fromDate) && before(date, this.toDate);
  isFrom = date => equals(date, this.fromDate);
  isTo = date => equals(date, this.toDate);
  constructor(
    element: ElementRef,
    private renderer: Renderer2,
    // tslint:disable-next-line:variable-name
    private _parserFormatter: NgbDateParserFormatter
  ) { }

  ngOnInit() {
    // Don't use + 1 in month, to show the start date starting one month ago
    this.startDate = { year: now.getFullYear(), month: now.getMonth(), day: now.getDate() };
    this.maxDate = { year: now.getFullYear() + 1, month: now.getMonth(), day: now.getDate() };
    this.minDate = { year: now.getFullYear() - 1, month: now.getMonth(), day: now.getDate() };
  }

  onInputChange(dateRange) {
    // Clear datepicker from input
    if (isNullOrEmptyOrUndefined(dateRange)) {
      this.fromDate = null;
      this.toDate = null;
    }
  }

  cleanInputRange() {
    this.form.get(this.filter.name).setValue('');
    this.range.emit({});
  }

  onDateSelection(date: NgbDateStruct) {
    let parsed = '';
    if (!this.fromDate && !this.toDate) {
      this.fromDate = date;
    } else if (this.fromDate && !this.toDate && after(date, this.fromDate)) {
      this.toDate = date;
      this.input.close();
    } else {
      this.toDate = null;
      this.fromDate = date;
    }
    if (this.fromDate) {
      parsed += this._parserFormatter.format(this.fromDate);
    }
    if (this.toDate) {
      parsed += ' - ' + this._parserFormatter.format(this.toDate);
    }
    this.renderer.setProperty(this.myRangeInput.nativeElement, 'value', parsed);
    if (this.fromDate && this.toDate) {
      const row = {
        from: this.fromDate.year + '-' + this.fromDate.month + '-' + this.fromDate.day,
        to: this.toDate.year + '-' + this.toDate.month + '-' + this.toDate.day
      };
      this.range.emit(row);
    } else {
      this.range.emit({
        from: this.fromDate.year + '-' + this.fromDate.month + '-' + this.fromDate.day
      });
    }
  }
}


