import { Component, OnInit, Input, EventEmitter, Output, OnChanges, ChangeDetectionStrategy } from '@angular/core';
import { FormGroupFilters } from '../../../models/table-filter.model';
import { FormGroup } from '@angular/forms';
import { isUndefined } from 'util';

@Component({
  selector: 'app-select-search-simple',
  templateUrl: './select-search-simple.component.html',
  styleUrls: ['./select-search-simple.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SelectSearchSimpleComponent implements OnInit, OnChanges {
  @Input() filter: FormGroupFilters;
  @Input() form: FormGroup;
  @Output() changeItem = new EventEmitter();

  list: any;
  constructor() { }

  ngOnInit() {
    this.generateList();
  }

  ngOnChanges() {
    if (this.filter.type === 'select-search-simple') {
      this.generateList();
    }
  }

  changeItems(id) {
    this.changeItem.emit({
      name: this.filter.name,
      id: (!isUndefined(id)) ? id.id : null
    });
  }

  generateList() {
    this.list = this.filter.options;
  }
}
