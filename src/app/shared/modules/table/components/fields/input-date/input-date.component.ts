import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';
import { FormGroupFilters } from '../../../models/table-filter.model';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-input-date',
  templateUrl: './input-date.component.html',
  styleUrls: ['./input-date.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class InputDateComponent implements OnInit {
  @Input() filter: FormGroupFilters;
  @Input() form: FormGroup;
  d2: any;

  constructor() { }

  ngOnInit() {

  }

}
