import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { SharedModule } from '../../shared.module';
import { TableLayoutComponent } from './components/table-layout/table-layout.component';
import { TableFiltersComponent } from './components/table-filters/table-filters.component';
import { SelectComponent } from './components/fields/select/select.component';
import { InputComponent } from './components/fields/input/input.component';
import { InputDateComponent } from './components/fields/input-date/input-date.component';
import { InputDateRangeComponent } from './components/fields/input-date-range/input-date-range.component';
import { SelectSearchMultipleComponent } from './components/fields/select-search-multiple/select-search-multiple.component';
import { SelectSearchSimpleComponent } from './components/fields/select-search-simple/select-search-simple.component';
import { DeleteButtonTableComponent } from './components/buttons/delete-button/delete-button.component';
import { ViewButtonTableComponent } from './components/buttons/view-button/view-button.component';
import { EditButtonTableComponent } from './components/buttons/edit-button/edit-button.component';
import { ExportButtonTableComponent } from './components/buttons/export-button/export-button.component';

@NgModule({
  declarations: [
    TableLayoutComponent,
    TableFiltersComponent,
    SelectComponent,
    InputComponent,
    InputDateComponent,
    InputDateRangeComponent,
    SelectSearchMultipleComponent,
    SelectSearchSimpleComponent,
    DeleteButtonTableComponent,
    ViewButtonTableComponent,
    EditButtonTableComponent,
    ExportButtonTableComponent
  ],
  imports: [
    CommonModule,
    SharedModule
  ],
  exports: [
    TableLayoutComponent,
    TableFiltersComponent
  ],
  providers: [
    DatePipe
  ]
})
export class TableModule { }
