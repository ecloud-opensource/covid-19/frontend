import { Routes } from '@angular/router';
// Private routes
export const PRIVATE_ROUTES: Routes = [
  {
    path: 'admin',
    loadChildren: () => import('../../modules/admin/admin.module').then(m => m.AdminModule)
  },
  {
    path: 'dashboard',
    loadChildren: () => import('../../modules/dashboard/dashboard.module').then(m => m.DashboardModule)
  },
  {
    path: 'colaborator',
    loadChildren: () => import('../../modules/colaborator/colaborator.module').then(m => m.ColaboratorModule)
  },
  {
    path: 'coming-soon',
    loadChildren: () => import('../../modules/coming-soon/coming-soon.module').then(m => m.ComingSoonModule)
  },
  {
    path: 'country',
    loadChildren: () => import('../../modules/country/country.module').then(m => m.CountryModule)
  },
  {
    path: 'state',
    loadChildren: () => import('../../modules/state/state.module').then(m => m.StateModule)
  },
  {
    path: 'city',
    loadChildren: () => import('../../modules/city/city.module').then(m => m.CityModule)
  },
  {
    path: 'link',
    loadChildren: () => import('../../modules/link/components/link.module').then(m => m.LinkModule)
  },
  {
    path: 'neighborhood',
    loadChildren: () => import('../../modules/neighborhood/neighborhood.module').then(m => m.NeighborhoodModule)
  },
  {
    path: 'settings',
    loadChildren: () => import('../../modules/setting/setting.module').then(m => m.SettingModule)
  },
  {
    path: 'case-record',
    loadChildren: () => import('../../modules/case-record/case-record.module').then(m => m.CaseRecordModule)
  },
  {
    path: 'city-feed',
    loadChildren: () => import('../../modules/city-feed/city-feed.module').then(m => m.CityFeedModule)
  },
  {
    path: 'misbehavior',
    loadChildren: () => import('../../modules/misbehavior/misbehavior.module').then(m => m.MisbehaviorModule)
  }
];
