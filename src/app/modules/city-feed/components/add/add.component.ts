import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastService } from 'src/app/core/services/toast/toast.service';
import { CityFeedService } from 'src/app/core/services/cityFeed/cityFeed.service';
import { CityFeed } from 'src/app/models/cityFeed';

@Component({
  selector: 'app-add-city-feed',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AddCityFeedComponent implements OnInit {
  parent: string;

  constructor(
    private cityFeedService: CityFeedService,
    private router: Router,
    private route: ActivatedRoute,
    public toast: ToastService
  ) {
    this.parent = this.route.snapshot.data.parent;
  }

  ngOnInit() {
  }

  getFormData(cityFeed) {
    this.cityFeedService.add<CityFeed>(cityFeed).subscribe(
      _ => {
        this.toast.success('modules.cityFeed.add.created');
        this.goBack();
      }
    );
  }

  goBack() {
    this.router.navigate([this.parent, 'list']);
  }

}
