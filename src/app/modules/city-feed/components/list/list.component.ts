import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { FormGroupFilters } from 'src/app/shared/modules/table/models/table-filter.model';
import { ColumnSetting } from 'src/app/shared/modules/table/models/table-layout.model';
import { BaseResponse } from 'src/app/models/base-response';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastService } from 'src/app/core/services/toast/toast.service';
import { SweetAlertService } from 'src/app/core/services/sweet-alert/sweet-alert.service';
import { EndpointsConstants } from 'src/app/helper/endpoints';
import { CityFeedService } from 'src/app/core/services/cityFeed/cityFeed.service';
import { CityFeed } from 'src/app/models/cityFeed';

@Component({
  selector: 'app-list-city-feed',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ListCityFeedComponent implements OnInit {
  tableActions = ['edit', 'delete'];
  parent: string;
  tableFilters: FormGroupFilters[] = [
    {
      name: 'term',
      placeholder: 'forms.fields.searchByName',
      type: 'input',
      defaultValue: ''
    }
  ];
  columnSettings: ColumnSetting[] = [
    {
      primaryKey: 'id',
      header: 'modules.cityFeed.list.id'
    },
    {
      primaryKey: 'title',
      header: 'modules.cityFeed.list.title'
    }
  ];
  filters: any[] = [];
  pageNumber = 1;
  results: BaseResponse<CityFeed>;

  constructor(
    private cdr: ChangeDetectorRef,
    private router: Router,
    private cityFeedService: CityFeedService,
    private toast: ToastService,
    private sweetAlertService: SweetAlertService,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.parent = this.route.snapshot.data.parent;
    this.updateList(1);
  }

  updateList(page: number) {
    this.pageNumber = page;
    this.cityFeedService.get<CityFeed>(page, EndpointsConstants.requestLimit, this.filters).subscribe(response => {
      this.results = new BaseResponse<CityFeed>(response, CityFeed);
      this.cdr.detectChanges();
    });
  }

  delete(id: number) {
    this.sweetAlertService.confirmAction('modules.cityFeed.list.deleteConfirmation').then(result => {
      if (result.value) {
        this.cityFeedService.delete(id).subscribe(
          _ => {
            this.toast.success('modules.cityFeed.list.deleted');
            this.updateList(this.pageNumber);
          }
        );
      }
    });
  }

  edit(id: number) {
    this.router.navigate([this.parent, 'edit', id]);
  }

  clickFilters(event) {
    this.filters = event;
    this.updateList(1);
  }

}
