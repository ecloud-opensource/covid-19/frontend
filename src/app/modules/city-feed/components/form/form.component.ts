import { Component, OnInit, OnChanges, Input, Output, EventEmitter, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CityFeed } from 'src/app/models/cityFeed';
import { CityService } from 'src/app/core/services/city/city.service';
import { EndpointsConstants } from 'src/app/helper/endpoints';
import { City } from 'src/app/models/city';
import { BaseResponse } from 'src/app/models/base-response';

@Component({
  selector: 'app-form-city-feed',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FormCityFeedComponent implements OnInit, OnChanges {
  routeTitle: string;
  parent: string;
  cityFeedForm: FormGroup;
  cities: BaseResponse<City>;

  @Input() currentCityFeed: CityFeed;

  @Output() sendFormData = new EventEmitter();

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private cityService: CityService,
    private cdr: ChangeDetectorRef,
  ) {
    this.parent = this.route.snapshot.data.parent;
    this.cityFeedForm = this.fb.group({
      url: ['', Validators.required],
      title: ['', Validators.required],
      image: ['', Validators.required],
      description: ['', Validators.required],
      order: ['', Validators.required],
      idCity: ['', Validators.required],
    });
  }

  ngOnInit() {
    this.routeTitle = this.route.snapshot.data.title;
    this.getCities(this.currentCityFeed ? this.currentCityFeed.city.name : '');
  }

  ngOnChanges() {
    if (this.currentCityFeed) {
      this.cityFeedForm.patchValue(this.currentCityFeed);
    }
  }

  getCities(term?) {
    const filters = {
      term: term ? term : ''
    };
    this.cityService.get<City>(1, EndpointsConstants.requestLimit, filters).subscribe(response => {
      this.cities = new BaseResponse<City>(response, City).data['rows'];
      this.cdr.detectChanges();
    });
  }

  onSubmit() {
    if (this.cityFeedForm.valid) {
      this.sendFormData.emit(this.cityFeedForm.value);
    }
  }

  changeFile(image) {
    this.cityFeedForm.get('image').setValue(image);
  }

  goBack() {
    this.router.navigate([this.parent, 'list']);
  }

}
