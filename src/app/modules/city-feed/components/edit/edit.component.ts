import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastService } from 'src/app/core/services/toast/toast.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { CityFeed } from 'src/app/models/cityFeed';
import { CityFeedService } from 'src/app/core/services/cityFeed/cityFeed.service';
import { CatalogApiScopes } from 'src/app/helper/catalog';

@Component({
  selector: 'app-edit-city-feed',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditCityFeedComponent implements OnInit {
  id: number;
  currentCityFeed$: Observable<CityFeed>;
  parent: string;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    public toast: ToastService,
    private cityFeedService: CityFeedService
  ) {
    this.parent = this.route.snapshot.data.parent;
  }

  ngOnInit() {
    this.id = +this.route.snapshot.paramMap.get('id');
    this.getCityFeed();
  }

  getCityFeed() {
    const filters = {
      scope: CatalogApiScopes.city
    };
    this.currentCityFeed$ = this.cityFeedService.getOne<CityFeed>(this.id, filters)
      .pipe(
        map(response => {
          this.id = response.data['id'];
          return new CityFeed(response.data);
        })
      );
  }

  getFormData(currentCityFeed: CityFeed) {
    this.cityFeedService.edit<CityFeed>(currentCityFeed, this.id).subscribe(_ => {
      this.toast.success('modules.cityFeed.edit.edited');
      this.goBack();
    });
  }

  goBack() {
    this.router.navigate([this.parent, 'list']);
  }

}
