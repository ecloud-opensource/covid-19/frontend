import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// Components
import { ListCityFeedComponent } from './components/list/list.component';
import { AddCityFeedComponent } from './components/add/add.component';
import { EditCityFeedComponent } from './components/edit/edit.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'list',
        component: ListCityFeedComponent,
        data: {
          title: 'modules.cityFeed.list.title',
          parent: 'city-feed'
        }
      },
      {
        path: 'add',
        component: AddCityFeedComponent,
        data: {
          title: 'modules.cityFeed.add.title',
          parent: 'city-feed'
        }
      },
      {
        path: 'edit/:id',
        component: EditCityFeedComponent,
        data: {
          title: 'modules.cityFeed.edit.title',
          parent: 'city-feed'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CityFeedRoutingModule { }
