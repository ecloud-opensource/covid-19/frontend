import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Modules
import { CityFeedRoutingModule } from './city-feed-routing.module';
import { TableModule } from 'src/app/shared/modules/table/table.module';
import { SharedModule } from 'src/app/shared/shared.module';

// Components
import { AddCityFeedComponent } from './components/add/add.component';
import { EditCityFeedComponent } from './components/edit/edit.component';
import { FormCityFeedComponent } from './components/form/form.component';
import { ListCityFeedComponent } from './components/list/list.component';


@NgModule({
  declarations: [
    AddCityFeedComponent,
    EditCityFeedComponent,
    FormCityFeedComponent,
    ListCityFeedComponent
  ],
  imports: [
    CommonModule,
    CityFeedRoutingModule,
    SharedModule,
    TableModule
  ]
})
export class CityFeedModule { }
