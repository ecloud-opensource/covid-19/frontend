import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// Components
import { ListAdminComponent } from './components/list/list.component';
import { AddAdminComponent } from './components/add/add.component';
import { EditAdminComponent } from './components/edit/edit.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'list',
        component: ListAdminComponent,
        data: {
          title: 'modules.admin.list.title',
          parent: 'admin'
        }
      },
      {
        path: 'add',
        component: AddAdminComponent,
        data: {
          title: 'modules.admin.add.title',
          parent: 'admin'
        }
      },
      {
        path: 'edit/:id',
        component: EditAdminComponent,
        data: {
          title: 'modules.admin.edit.title',
          parent: 'admin'
        }
      },
      {
        path: 'profile',
        component: EditAdminComponent,
        data: {
          title: 'modules.admin.profile.title',
          parent: 'admin',
          isProfile: true
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
