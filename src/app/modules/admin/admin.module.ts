import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Modules
import { AdminRoutingModule } from './admin-routing.module';
import { TableModule } from 'src/app/shared/modules/table/table.module';
import { SharedModule } from 'src/app/shared/shared.module';

// Components
import { AddAdminComponent } from './components/add/add.component';
import { EditAdminComponent } from './components/edit/edit.component';
import { FormAdminComponent } from './components/form/form.component';
import { ListAdminComponent } from './components/list/list.component';


@NgModule({
  declarations: [
    AddAdminComponent,
    EditAdminComponent,
    FormAdminComponent,
    ListAdminComponent
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    SharedModule,
    TableModule
  ]
})
export class AdminModule { }
