import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Admin } from 'src/app/models/admin';
import { ToastService } from 'src/app/core/services/toast/toast.service';
import { AdminService } from 'src/app/core/services/admin/admin.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AuthService } from 'src/app/core/services/auth/auth.service';

@Component({
  selector: 'app-edit-admin',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditAdminComponent implements OnInit {
  id: number;
  currentAdmin$: Observable<Admin>;
  parent: string;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    public toast: ToastService,
    private authService: AuthService,
    private adminService: AdminService
  ) {
    this.parent = this.route.snapshot.data.parent;
  }

  ngOnInit() {
    this.id = +this.route.snapshot.paramMap.get('id');
    this.getAdmin();
  }

  getAdmin() {
    if (this.route.snapshot.data.isProfile) {
      this.currentAdmin$ = this.adminService.getMe().pipe(
        map( response => {
          return new Admin(response.data);
        })
      );
    } else {
      this.currentAdmin$ = this.adminService.getOne<Admin>(this.id)
      .pipe (
        map( response => {
          this.id = response.data['id'];
          return new Admin(response.data);
        })
      );
    }
  }

  getFormData(currentAdmin: Admin) {
    if (this.route.snapshot.data.isProfile) {
      currentAdmin.id = this.id;
      this.adminService.putMe(currentAdmin).subscribe( _ => {
        this.toast.success('modules.admin.edit.profileEdited');
        // Update operator
        this.authService.setCurrentAdmin(currentAdmin);
        this.goBack();
      });
    } else {
      this.adminService.edit<Admin>(currentAdmin, this.id).subscribe( _ => {
        this.toast.success('modules.admin.edit.edited');
        this.goBack();
      });
    }
  }

  goBack() {
    this.router.navigate([this.parent, 'list']);
  }

}
