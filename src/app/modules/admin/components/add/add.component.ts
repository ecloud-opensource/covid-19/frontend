import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { AdminService } from 'src/app/core/services/admin/admin.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastService } from 'src/app/core/services/toast/toast.service';
import { Admin } from 'src/app/models/admin';

@Component({
  selector: 'app-add-admin',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AddAdminComponent implements OnInit {
  parent: string;

  constructor(
    private adminService: AdminService,
    private router: Router,
    private route: ActivatedRoute,
    public toast: ToastService
  ) {
    this.parent = this.route.snapshot.data.parent;
  }

  ngOnInit() {
  }

  getFormData(admin) {
    this.adminService.add<Admin>(admin).subscribe(
      _ => {
        this.toast.success('modules.admin.add.created');
        this.goBack();
      }
    );
  }

  goBack() {
    this.router.navigate([this.parent, 'list']);
  }

}
