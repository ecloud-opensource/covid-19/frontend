import { Component, OnInit, OnChanges, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { removeValidators, isEmpty } from 'src/app/helper/utils';
import { Admin } from 'src/app/models/admin';
import { DefaultAvatarUrl } from 'src/app/helper/config';

@Component({
  selector: 'app-form-admin',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FormAdminComponent implements OnInit, OnChanges {
  routeTitle: string;
  parent: string;
  adminForm: FormGroup;

  @Input() currentAdmin: Admin;

  @Output() sendFormData = new EventEmitter();

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder
  ) {
    this.parent = this.route.snapshot.data.parent;
    this.adminForm = this.fb.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      identification: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      avatarUrl: ['', Validators.required],
      phone: ['', Validators.required],
      profession: ['', Validators.required],
      workplace: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(15)]]
    });
  }

  ngOnInit() {
    this.routeTitle = this.route.snapshot.data.title;
  }

  ngOnChanges() {
    if (this.currentAdmin) {
      this.adminForm.patchValue(this.currentAdmin);
      removeValidators(this.adminForm, 'password');
      this.adminForm.get('password').setValue('');
    }
  }

  onSubmit() {
    if (this.adminForm.valid) {
      const formModel = this.adminForm.value;
      const admin = {
        firstName: formModel.firstName,
        lastName: formModel.lastName,
        identification: formModel.identification,
        email: formModel.email,
        phone: formModel.phone,
        profession: formModel.profession,
        workplace: formModel.workplace,
        avatarUrl: formModel.avatarUrl !== DefaultAvatarUrl ? formModel.avatarUrl : undefined,
        password: (!isEmpty(formModel.password)) ? formModel.password : undefined
      };
      this.sendFormData.emit(admin);
    }
  }

  changeFile(avatarUrl) {
    this.adminForm.get('avatarUrl').setValue(avatarUrl);
  }

  goBack() {
    this.router.navigate([this.parent, 'list']);
  }

}
