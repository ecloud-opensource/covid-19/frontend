import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { FormGroupFilters } from 'src/app/shared/modules/table/models/table-filter.model';
import { ColumnSetting } from 'src/app/shared/modules/table/models/table-layout.model';
import { BaseResponse } from 'src/app/models/base-response';
import { Admin } from 'src/app/models/admin';
import { Router } from '@angular/router';
import { AdminService } from 'src/app/core/services/admin/admin.service';
import { ToastService } from 'src/app/core/services/toast/toast.service';
import { SweetAlertService } from 'src/app/core/services/sweet-alert/sweet-alert.service';
import { EndpointsConstants } from 'src/app/helper/endpoints';

@Component({
  selector: 'app-list-admin',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ListAdminComponent implements OnInit {
  tableActions = ['edit', 'delete'];
  tableFilters: FormGroupFilters[] = [
    {
      name: 'search',
      placeholder: 'forms.fields.searchByName',
      type: 'input',
      defaultValue: ''
    }
  ];
  columnSettings: ColumnSetting[] = [
    {
      primaryKey: 'id',
      header: 'modules.admin.list.id'
    },
    {
      primaryKey: 'fullName',
      header: 'modules.admin.list.name'
    },
    {
      primaryKey: 'email',
      header: 'modules.admin.list.email'
    }
  ];
  filters: any[] = [];
  pageNumber = 1;
  results: BaseResponse<Admin>;

  constructor(
    private cdr: ChangeDetectorRef,
    private router: Router,
    private adminService: AdminService,
    private toast: ToastService,
    private sweetAlertService: SweetAlertService
  ) { }

  ngOnInit() {
    this.updateList(1);
  }

  updateList(page: number) {
    this.pageNumber = page;
    this.adminService.get<Admin>(page, EndpointsConstants.requestLimit, this.filters).subscribe( response => {
      this.results = new BaseResponse<Admin>(response, Admin);
      this.cdr.detectChanges();
    });
  }

  delete(id: number) {
    this.sweetAlertService.confirmAction('modules.admin.list.deleteConfirmation').then( result => {
      if (result.value) {
        this.adminService.delete(id).subscribe(
          _ => {
            this.toast.success('modules.admin.list.deleted');
            this.updateList(this.pageNumber);
          }
        );
      }
    });
  }

  edit(id: number) {
    this.router.navigate(['admin', 'edit', id]);
  }

  clickFilters(event) {
    this.filters = event;
    this.updateList(1);
  }

}
