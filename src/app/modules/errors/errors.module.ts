import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ErrorsRoutingModule } from './errors-routing.module';
import { Error404Component } from './components/error-404/error-404.component';
import { Error401Component } from './components/error-401/error-401.component';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [
    Error404Component,
    Error401Component
  ],
  imports: [
    CommonModule,
    ErrorsRoutingModule,
    SharedModule
  ]
})
export class ErrorsModule { }
