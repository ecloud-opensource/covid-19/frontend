import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-error-401',
  templateUrl: './error-401.component.html',
  styleUrls: ['./error-401.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class Error401Component implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  goToLogin() {
    this.router.navigate(['/auth', 'login']);
  }
}
