import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Error404Component } from './components/error-404/error-404.component';
import { Error401Component } from './components/error-401/error-401.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '404',
        component: Error404Component
      },
      {
        path: '401',
        component: Error401Component
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ErrorsRoutingModule { }
