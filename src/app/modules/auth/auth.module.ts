// Modules
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { AuthRoutingModule } from './auth-routing.module';
// Components
import { LoginAuthComponent } from './components/login/login.component';
import { ForgotPasswordAuthComponent } from './components/forgot-password/forgot-password.component';
import { ResetPasswordAuthComponent } from './components/reset-password/reset-password.component';
import { RegisterAuthComponent } from './components/register/register.component';

@NgModule({
  declarations: [
    LoginAuthComponent,
    ForgotPasswordAuthComponent,
    ResetPasswordAuthComponent,
    RegisterAuthComponent
  ],
  imports: [
    CommonModule,
    AuthRoutingModule,
    SharedModule
  ]
})
export class AuthModule { }
