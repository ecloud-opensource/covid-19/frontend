import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginAuthComponent } from './components/login/login.component';
import { ForgotPasswordAuthComponent } from './components/forgot-password/forgot-password.component';
import { ResetPasswordAuthComponent } from './components/reset-password/reset-password.component';
import { RegisterAuthComponent } from './components/register/register.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'login',
        component: LoginAuthComponent
      },
      {
        path: 'forgot-password',
        component: ForgotPasswordAuthComponent
      },
      {
        path: 'reset-password/:token',
        component: ResetPasswordAuthComponent
      },
      {
        path: 'register',
        component: RegisterAuthComponent
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }
