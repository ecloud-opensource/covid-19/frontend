import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastService } from 'src/app/core/services/toast/toast.service';
import { AuthService } from 'src/app/core/services/auth/auth.service';
import { passwordMatchValidator } from 'src/app/helper/validators';

@Component({
  selector: 'app-register-auth',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RegisterAuthComponent implements OnInit {
  registerForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private toast: ToastService,
    private authService: AuthService
  ) {
    this.registerForm = this.fb.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      phone: ['', Validators.required],
      identification: ['', Validators.required],
      profession: ['', Validators.required],
      workplace: ['', Validators.required],
      email: ['', Validators.required],
      password: ['', Validators.required],
      repeatPassword: ['', Validators.required]
    }, { validator: passwordMatchValidator });
  }

  ngOnInit() {
  }

  onSubmit() {
    /*
    if (this.registerForm.valid) {
      this.authService.register(this.registerForm.value).subscribe( _ => {
        this.toast.success('modules.auth.register.adminRegistered');
        this.goBack();
      });
    }
    */
  }

  goBack() {
    this.router.navigate(['/auth', 'login']);
  }
}
