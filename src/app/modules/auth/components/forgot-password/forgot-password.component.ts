import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ToastService } from 'src/app/core/services/toast/toast.service';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/core/services/auth/auth.service';

@Component({
  selector: 'app-forgot-password-auth',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ForgotPasswordAuthComponent implements OnInit {
  forgotPasswordForm: FormGroup;

  constructor(
    private router: Router,
    private toast: ToastService,
    private fb: FormBuilder,
    private authService: AuthService
  ) {
    if (this.authService.isAuthenticated(false)) { this.router.navigate(['/dashboard']); }

    this.forgotPasswordForm = this.fb.group({
      email: ['']
    });
  }

  ngOnInit() {
  }

  onSubmit() {
    if (this.forgotPasswordForm.valid) {
      const formModel = this.forgotPasswordForm.value;
      this.authService.forgotPassword(formModel.email).subscribe( _ => {
        this.toast.info('modules.auth.forgotPassword.mailSent');
        this.router.navigate(['/auth', 'login']);
      });
    }
  }

}
