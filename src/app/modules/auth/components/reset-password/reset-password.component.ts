import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { passwordMatchValidator } from 'src/app/helper/validators';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastService } from 'src/app/core/services/toast/toast.service';
import { AuthService } from 'src/app/core/services/auth/auth.service';

@Component({
  selector: 'app-reset-password-auth',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ResetPasswordAuthComponent implements OnInit {
  resetPasswordForm: FormGroup;
  token: string;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private toast: ToastService,
    private authService: AuthService
  ) {
    if (this.authService.isAuthenticated(false)) { this.router.navigate(['/dashboard']); }

    this.token = this.route.snapshot.paramMap.get('token');

    this.resetPasswordForm = this.fb.group({
      password: [''],
      repeatPassword: ['']
    }, { validator: passwordMatchValidator });
  }

  ngOnInit() {
  }

  onSubmit() {
    if (this.resetPasswordForm.valid) {
      const formModel = this.resetPasswordForm.value;
      this.authService.restorePassword(formModel.password, this.token).subscribe( _ => {
        this.toast.success('modules.auth.resetPassword.passwordChanged');
        this.router.navigate(['/auth', 'login']);
      });
    }
  }

}
