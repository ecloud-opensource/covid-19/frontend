import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SettingRoutingModule } from './setting-routing.module';
import { EditSettingComponent } from './components/edit/edit.component';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [EditSettingComponent],
  imports: [
    CommonModule,
    SettingRoutingModule,
    SharedModule
  ]
})
export class SettingModule { }
