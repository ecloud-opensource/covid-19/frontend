import { Component, OnInit, ChangeDetectionStrategy, Output, EventEmitter, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { BaseResponse } from 'src/app/models/base-response';
import { Setting } from 'src/app/models/setting';
import { ActivatedRoute } from '@angular/router';
import { ToastService } from 'src/app/core/services/toast/toast.service';
import { SettingService } from 'src/app/core/services/setting/setting.service';

@Component({
  selector: 'app-edit-setting',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditSettingComponent implements OnInit {
  routeTitle: string;
  settingForm: FormGroup = new FormGroup({});
  settings: BaseResponse<Setting>;

  @Output() sendFormData = new EventEmitter();

  constructor(
    private fb: FormBuilder,
    private cdr: ChangeDetectorRef,
    private route: ActivatedRoute,
    private toast: ToastService,
    private settingService: SettingService
  ) { }

  ngOnInit() {
    this.routeTitle = this.route.snapshot.data.title;
    this.getSettings();
  }

  getSettings() {
    this.settingService.get<Setting>().subscribe( response => {
      this.settings = new BaseResponse<Setting>(response, Setting);
      this.toFormGroup(this.settings.data['rows']);
      this.cdr.detectChanges();
    });
  }

  createFormGroup(setting): FormGroup {
    return this.fb.group({
      id: setting.id,
      name: setting.name,
      code: setting.code,
      description: setting.description,
      value: setting.value,
      showButton: false
    });
  }

  toFormGroup(formControls) {
    formControls.forEach( control => {
      this.settingForm.addControl(control.code, this.createFormGroup(control));
    });
  }

  updateSetting(setting) {
    const formModel = this.settingForm.get(setting.code).get('value').value;
    const currentSetting = new Setting({
      value: formModel
    });
    this.settingService.edit<Setting>(currentSetting, setting.id).subscribe( _ => {
      this.toast.success('modules.setting.edit.edited');
    });
  }

  hideShowButton(settingCode) {
    this.settingForm.get(settingCode).get('showButton').setValue(true);
  }
}
