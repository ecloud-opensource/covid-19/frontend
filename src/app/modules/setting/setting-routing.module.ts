import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EditSettingComponent } from './components/edit/edit.component';

const routes: Routes = [
  {
    path: 'edit',
    component: EditSettingComponent,
    data: {
      title: 'modules.setting.edit.title',
      parent: '/settings'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SettingRoutingModule { }
