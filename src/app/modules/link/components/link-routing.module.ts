import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EditLinkComponent } from './edit/edit.component';
import { ListLinkComponent } from './list/list.component';
import { AddLinkComponent } from './add/add.component';

const routes: Routes = [
    {
        path: '',
        children: [
        {
            path: 'add',
            component: AddLinkComponent,
            data: {
                title: 'modules.link.add.title',
                parent: 'link'
            }
        },
        {
            path: 'list',
            component: ListLinkComponent,
            data: {
                title: 'modules.link.list.title',
                parent: 'link'
            }
        },
        {
            path: 'edit/:id',
            component: EditLinkComponent,
            data: {
            title: 'modules.link.edit.title',
            parent: 'link'
            }
        }]
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LinkRoutingModule { }
