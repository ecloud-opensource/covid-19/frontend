import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { AdminService } from 'src/app/core/services/admin/admin.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastService } from 'src/app/core/services/toast/toast.service';
import { Admin } from 'src/app/models/admin';
import { LinkService } from 'src/app/core/services/link/link.service';
import { Link } from 'src/app/models/link';

@Component({
  selector: 'app-add-link',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AddLinkComponent implements OnInit {
  parent: string;

  constructor(
    private linkService: LinkService,
    private router: Router,
    private route: ActivatedRoute,
    public toast: ToastService
  ) {
    this.parent = this.route.snapshot.data.parent;
  }

  ngOnInit() {
  }

  getFormData(link) {
    this.linkService.add<Link>(link).subscribe(
      _ => {
        this.toast.success('modules.link.add.created');
        this.goBack();
      }
    );
  }

  goBack() {
    this.router.navigate([this.parent, 'list']);
  }

}
