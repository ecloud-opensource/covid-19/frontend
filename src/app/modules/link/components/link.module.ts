import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { TableModule } from 'src/app/shared/modules/table/table.module';
import { LinkRoutingModule } from './link-routing.module';
import { ListLinkComponent } from './list/list.component';
import { FormLinkComponent } from './form/form.component';
import { EditLinkComponent } from './edit/edit.component';
import { AddLinkComponent } from './add/add.component';

@NgModule({
  declarations: [
    EditLinkComponent,
    FormLinkComponent,
    ListLinkComponent,
    AddLinkComponent,
  ],
  imports: [
    CommonModule,
    LinkRoutingModule,
    SharedModule,
    TableModule
  ]
})
export class LinkModule { }
