import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { FormGroupFilters } from 'src/app/shared/modules/table/models/table-filter.model';
import { ColumnSetting } from 'src/app/shared/modules/table/models/table-layout.model';
import { BaseResponse } from 'src/app/models/base-response';
import { Router } from '@angular/router';
import { ToastService } from 'src/app/core/services/toast/toast.service';
import { SweetAlertService } from 'src/app/core/services/sweet-alert/sweet-alert.service';
import { EndpointsConstants } from 'src/app/helper/endpoints';
import { Link } from 'src/app/models/link';
import { LinkService } from 'src/app/core/services/link/link.service';

@Component({
  selector: 'app-list-link',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ListLinkComponent implements OnInit {
  tableActions = ['edit', 'delete'];
  tableFilters: FormGroupFilters[] = [
    {
      name: 'term',
      placeholder: 'forms.fields.searchByTitle',
      type: 'input',
      defaultValue: ''
    }
  ];
  columnSettings: ColumnSetting[] = [
    {
      primaryKey: 'id',
      header: 'modules.link.list.id'
    },
    {
      primaryKey: 'title',
      header: 'modules.link.list.titleFieldTable'
    }
  ];
  filters: any[] = [];
  pageNumber = 1;
  results: BaseResponse<Link>;

  constructor(
    private cdr: ChangeDetectorRef,
    private router: Router,
    private linkService: LinkService,
    private toast: ToastService,
    private sweetAlertService: SweetAlertService
  ) { }

  ngOnInit() {
    this.updateList(1);
  }

  updateList(page: number) {
    this.pageNumber = page;
    this.linkService.get<Link>(page, EndpointsConstants.requestLimit, this.filters).subscribe( response => {
      this.results = new BaseResponse<Link>(response, Link);
      this.cdr.detectChanges();
    });
  }

  delete(id: number) {
    this.sweetAlertService.confirmAction('modules.link.list.deleteConfirmation').then( result => {
      if (result.value) {
        this.linkService.delete(id).subscribe(
          _ => {
            this.toast.success('modules.link.list.deleted');
            this.updateList(this.pageNumber);
          }
        );
      }
    });
  }

  edit(id: number) {
    this.router.navigate(['link', 'edit', id]);
  }

  clickFilters(event) {
    this.filters = event;
    this.updateList(1);
  }

}
