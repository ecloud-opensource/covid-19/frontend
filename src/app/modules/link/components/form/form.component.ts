import { Component, OnInit, OnChanges, Input, Output, EventEmitter, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Link } from '../../../../models/link';
import { CityService } from 'src/app/core/services/city/city.service';
import { City } from 'src/app/models/city';
import { BaseResponse } from 'src/app/models/base-response';
import { EndpointsConstants } from 'src/app/helper/endpoints';

@Component({
  selector: 'app-form-link',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FormLinkComponent implements OnInit, OnChanges {
  routeTitle: string;
  parent: string;
  linkForm: FormGroup;
  cities: BaseResponse<City>;

  @Input() currentLink: Link;
  @Input() isAddForm = false;

  @Output() sendFormData = new EventEmitter();

  constructor(
    private cdr: ChangeDetectorRef,
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private cityService: CityService
  ) {
    this.parent = this.route.snapshot.data.parent;
    this.linkForm = this.fb.group({
      url: ['', Validators.required],
      title: ['', Validators.required],
      image: ['', Validators.required],
      description: ['', Validators.required],
      order: ['', Validators.required],
      idCity: [null, Validators.required]
    });
  }

  ngOnInit() {
    this.routeTitle = this.route.snapshot.data.title;
    this.getCities(this.currentLink ? this.currentLink.city.name : '');
  }

  ngOnChanges() {
    if (this.currentLink) {
      this.linkForm.patchValue(this.currentLink);
    }
  }

  getCities(term?) {
    const filters = {
      term: term ? term : ''
    };
    this.cityService.get<City>(1, EndpointsConstants.requestLimit, filters).subscribe(response => {
      this.cities = new BaseResponse<City>(response, City).data['rows'];
      this.cdr.detectChanges();
    });
  }

  onSubmit() {
    if (this.linkForm.valid) {
      this.sendFormData.emit(this.linkForm.value);
    }
  }

  changeFile(image) {
    this.linkForm.get('image').setValue(image);
  }

  goBack() {
    this.router.navigate([this.parent, 'list']);
  }

}
