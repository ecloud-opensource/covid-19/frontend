import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastService } from 'src/app/core/services/toast/toast.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Link } from 'src/app/models/link';
import { LinkService } from 'src/app/core/services/link/link.service';
import { CatalogApiScopes } from 'src/app/helper/catalog';

@Component({
  selector: 'app-edit-link',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditLinkComponent implements OnInit {
  id: number;
  currentLink$: Observable<Link>;
  parent: string;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    public toast: ToastService,
    public linkService: LinkService
  ) {
    this.parent = this.route.snapshot.data.parent;
  }

  ngOnInit() {
    this.id = +this.route.snapshot.paramMap.get('id');
    this.getLink();
  }

  getLink() {
    const filters = {
      scope: CatalogApiScopes.city
    };
    this.currentLink$ = this.linkService.getOne<Link>(this.id, filters)
    .pipe (
      map( response => {
        return new Link(response.data);
      })
    );
  }

  getFormData(currentLink: Link) {
    this.linkService.edit<Link>(currentLink, this.id).subscribe( _ => {
      this.toast.success('modules.link.edit.edited');
      this.goBack();
    });
  }

  goBack() {
    this.router.navigate([this.parent, 'list']);
  }

}
