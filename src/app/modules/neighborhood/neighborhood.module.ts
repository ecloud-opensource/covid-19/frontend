import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NeighborhoodRoutingModule } from './neighborhood-routing.module';
import { EditNeighborhoodComponent } from './components/edit/edit.component';
import { FormNeighborhoodComponent } from './components/form/form.component';
import { ListNeighborhoodComponent } from './components/list/list.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { TableModule } from 'src/app/shared/modules/table/table.module';
import { MapModule } from 'src/app/shared/modules/map/map.module';

@NgModule({
  declarations: [
    EditNeighborhoodComponent,
    FormNeighborhoodComponent,
    ListNeighborhoodComponent
  ],
  imports: [
    CommonModule,
    NeighborhoodRoutingModule,
    SharedModule,
    TableModule,
    MapModule
  ]
})
export class NeighborhoodModule { }
