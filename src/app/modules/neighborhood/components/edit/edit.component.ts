import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Observable } from 'rxjs';
import { Neighborhood } from 'src/app/models/neighborhood';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastService } from 'src/app/core/services/toast/toast.service';
import { NeighborhoodService } from 'src/app/core/services/neighborhood/neighborhood.service';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-edit-neighborhood',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditNeighborhoodComponent implements OnInit {
  id: number;
  currentNeighborhood$: Observable<Neighborhood>;
  parent: string;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    public toast: ToastService,
    private neighborhoodService: NeighborhoodService
  ) {
    this.parent = this.route.snapshot.data.parent;
  }

  ngOnInit() {
    this.id = +this.route.snapshot.paramMap.get('id');
    this.getNeighborhood();
  }

  getNeighborhood() {
    this.currentNeighborhood$ = this.neighborhoodService.getOne<Neighborhood>(this.id)
    .pipe (
      map( response => {
        return new Neighborhood(response.data);
      })
    );
  }

  getFormData(currentNeighborhood: Neighborhood) {
    this.neighborhoodService.edit<Neighborhood>(currentNeighborhood, this.id).subscribe( _ => {
      this.toast.success('modules.neighborhood.edit.edited');
      this.goBack();
    });
  }

  goBack() {
    this.router.navigate([this.parent, 'list']);
  }

}
