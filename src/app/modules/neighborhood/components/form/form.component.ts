import { Component, OnInit, ChangeDetectionStrategy, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Neighborhood } from 'src/app/models/neighborhood';
import { Router, ActivatedRoute } from '@angular/router';
import { MapZoomNeighborhood } from 'src/app/helper/config';

@Component({
  selector: 'app-form-neighborhood',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FormNeighborhoodComponent implements OnInit, OnChanges {
  routeTitle: string;
  parent: string;
  neighborhoodForm: FormGroup;
  zoom = MapZoomNeighborhood;
  marker;

  @Input() currentNeighborhood: Neighborhood;

  @Output() sendFormData = new EventEmitter();

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder
  ) {
    this.parent = this.route.snapshot.data.parent;
    this.neighborhoodForm = this.fb.group({
      name: ['', Validators.required],
      code: ['', Validators.required],
      center: ['', Validators.required],
      area: ['', Validators.required],
      idCity: ['', Validators.required]
    });
  }

  ngOnInit() {
    this.routeTitle = this.route.snapshot.data.title;
  }

  ngOnChanges() {
    if (this.currentNeighborhood) {
      this.marker = {
        latitude: this.currentNeighborhood.center.coordinates[0],
        longitude: this.currentNeighborhood.center.coordinates[1]
      };
      this.neighborhoodForm.patchValue(this.currentNeighborhood);
    }
  }

  onSubmit() {
    if (this.neighborhoodForm.valid) {
      this.sendFormData.emit(this.neighborhoodForm.value);
    }
  }

  goBack() {
    this.router.navigate([this.parent, 'list']);
  }

}
