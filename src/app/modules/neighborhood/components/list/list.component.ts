import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { FormGroupFilters } from 'src/app/shared/modules/table/models/table-filter.model';
import { ColumnSetting } from 'src/app/shared/modules/table/models/table-layout.model';
import { BaseResponse } from 'src/app/models/base-response';
import { Neighborhood } from 'src/app/models/neighborhood';
import { NeighborhoodService } from 'src/app/core/services/neighborhood/neighborhood.service';
import { Router } from '@angular/router';
import { EndpointsConstants } from 'src/app/helper/endpoints';
import { CatalogApiScopes } from 'src/app/helper/catalog';

@Component({
  selector: 'app-list-neighborhood',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ListNeighborhoodComponent implements OnInit {
  tableActions = ['edit'];
  tableFilters: FormGroupFilters[] = [
    {
      name: 'term',
      placeholder: 'forms.fields.searchByName',
      type: 'input',
      defaultValue: ''
    }
  ];
  columnSettings: ColumnSetting[] = [
    {
      primaryKey: 'id',
      header: 'modules.neighborhood.list.id'
    },
    {
      primaryKey: 'name',
      header: 'modules.neighborhood.list.name'
    },
    {
      primaryKey: 'city.name',
      header: 'modules.neighborhood.list.city'
    }
  ];
  filters: any[] = [];
  pageNumber = 1;
  results: BaseResponse<Neighborhood>;

  constructor(
    private cdr: ChangeDetectorRef,
    private router: Router,
    private neighborhoodService: NeighborhoodService
  ) { }

  ngOnInit() {
    this.updateList(1);
  }

  updateList(page: number) {
    this.pageNumber = page;
    const filters = {
      ...this.filters,
      scope: CatalogApiScopes.city
    };
    this.neighborhoodService.get<Neighborhood>(page, EndpointsConstants.requestLimit, filters).subscribe( response => {
      this.results = new BaseResponse<Neighborhood>(response, Neighborhood);
      this.cdr.detectChanges();
    });
  }

  edit(id: number) {
    this.router.navigate(['neighborhood', 'edit', id]);
  }

  clickFilters(event) {
    this.filters = event;
    this.updateList(1);
  }

}
