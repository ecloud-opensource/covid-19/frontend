
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListNeighborhoodComponent } from './components/list/list.component';
import { EditNeighborhoodComponent } from './components/edit/edit.component';


const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'list',
        component: ListNeighborhoodComponent,
        data: {
          title: 'modules.neighborhood.list.title',
          parent: 'neighborhood'
        }
      },
      {
        path: 'edit/:id',
        component: EditNeighborhoodComponent,
        data: {
          title: 'modules.neighborhood.edit.title',
          parent: 'neighborhood'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NeighborhoodRoutingModule { }
