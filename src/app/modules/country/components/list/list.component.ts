import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { FormGroupFilters } from 'src/app/shared/modules/table/models/table-filter.model';
import { ColumnSetting } from 'src/app/shared/modules/table/models/table-layout.model';
import { BaseResponse } from 'src/app/models/base-response';
import { Country } from 'src/app/models/country';
import { CountryService } from 'src/app/core/services/country/country.service';
import { Router } from '@angular/router';
import { EndpointsConstants } from 'src/app/helper/endpoints';

@Component({
  selector: 'app-list-country',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ListCountryComponent implements OnInit {
  tableActions = ['edit'];
  tableFilters: FormGroupFilters[] = [
    {
      name: 'term',
      placeholder: 'forms.fields.searchByName',
      type: 'input',
      defaultValue: ''
    }
  ];
  columnSettings: ColumnSetting[] = [
    {
      primaryKey: 'id',
      header: 'modules.country.list.id'
    },
    {
      primaryKey: 'name',
      header: 'modules.country.list.name'
    }
  ];
  filters: any[] = [];
  pageNumber = 1;
  results: BaseResponse<Country>;

  constructor(
    private cdr: ChangeDetectorRef,
    private router: Router,
    private countryService: CountryService
  ) { }

  ngOnInit() {
    this.updateList(1);
  }

  updateList(page: number) {
    this.pageNumber = page;
    this.countryService.get<Country>(page, EndpointsConstants.requestLimit, this.filters).subscribe( response => {
      this.results = new BaseResponse<Country>(response, Country);
      this.cdr.detectChanges();
    });
  }

  edit(id: number) {
    this.router.navigate(['country', 'edit', id]);
  }

  clickFilters(event) {
    this.filters = event;
    this.updateList(1);
  }

}
