import { Component, OnInit, ChangeDetectionStrategy, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Country } from 'src/app/models/country';
import { Router, ActivatedRoute } from '@angular/router';
import { MapZoomCountry } from 'src/app/helper/config';

@Component({
  selector: 'app-form-country',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FormCountryComponent implements OnInit, OnChanges {
  routeTitle: string;
  parent: string;
  countryForm: FormGroup;
  zoom = MapZoomCountry;
  marker;

  @Input() currentCountry: Country;

  @Output() sendFormData = new EventEmitter();

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder
  ) {
    this.parent = this.route.snapshot.data.parent;
    this.countryForm = this.fb.group({
      name: ['', Validators.required],
      code: ['', Validators.required],
      center: ['', Validators.required],
      area: ['', Validators.required]
    });
  }

  ngOnInit() {
    this.routeTitle = this.route.snapshot.data.title;
  }

  ngOnChanges() {
    if (this.currentCountry) {
      this.marker = {
        latitude: this.currentCountry.center.coordinates[0],
        longitude: this.currentCountry.center.coordinates[1]
      };
      this.countryForm.patchValue(this.currentCountry);
    }
  }

  onSubmit() {
    if (this.countryForm.valid) {
      this.sendFormData.emit(this.countryForm.value);
    }
  }

  goBack() {
    this.router.navigate([this.parent, 'list']);
  }

}
