import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Observable } from 'rxjs';
import { Country } from 'src/app/models/country';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastService } from 'src/app/core/services/toast/toast.service';
import { CountryService } from 'src/app/core/services/country/country.service';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-edit-country',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditCountryComponent implements OnInit {
  id: number;
  currentCountry$: Observable<Country>;
  parent: string;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    public toast: ToastService,
    private countryService: CountryService
  ) {
    this.parent = this.route.snapshot.data.parent;
  }

  ngOnInit() {
    this.id = +this.route.snapshot.paramMap.get('id');
    this.getCountry();
  }

  getCountry() {
    this.currentCountry$ = this.countryService.getOne<Country>(this.id)
    .pipe (
      map( response => {
        return new Country(response.data);
      })
    );
  }

  getFormData(currentCountry: Country) {
    this.countryService.edit<Country>(currentCountry, this.id).subscribe( _ => {
      this.toast.success('modules.country.edit.edited');
      this.goBack();
    });
  }

  goBack() {
    this.router.navigate([this.parent, 'list']);
  }

}
