import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CountryRoutingModule } from './country-routing.module';
import { EditCountryComponent } from './components/edit/edit.component';
import { FormCountryComponent } from './components/form/form.component';
import { ListCountryComponent } from './components/list/list.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { TableModule } from 'src/app/shared/modules/table/table.module';
import { MapModule } from 'src/app/shared/modules/map/map.module';


@NgModule({
  declarations: [
    EditCountryComponent,
    FormCountryComponent,
    ListCountryComponent
  ],
  imports: [
    CommonModule,
    CountryRoutingModule,
    SharedModule,
    TableModule,
    MapModule
  ]
})
export class CountryModule { }
