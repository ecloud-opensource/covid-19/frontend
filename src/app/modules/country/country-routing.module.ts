import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListCountryComponent } from './components/list/list.component';
import { EditCountryComponent } from './components/edit/edit.component';


const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'list',
        component: ListCountryComponent,
        data: {
          title: 'modules.country.list.title',
          parent: 'country'
        }
      },
      {
        path: 'edit/:id',
        component: EditCountryComponent,
        data: {
          title: 'modules.country.edit.title',
          parent: 'country'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CountryRoutingModule { }
