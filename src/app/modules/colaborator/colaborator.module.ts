import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ColaboratorRoutingModule } from './colaborator-routing.module';
import { DashboardColaboratorComponent } from './components/dashboard/dashboard.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { MapModule } from 'src/app/shared/modules/map/map.module';
import { TableModule } from 'src/app/shared/modules/table/table.module';
import { ProfileColaboratorComponent } from './components/profile/profile.component';
import { DetailsColaboratorComponent } from './components/details/details.component';
import { ListCardColaboratorComponent } from './components/list-card/list-card.component';
import { EditColaboratorComponent } from './components/edit/edit.component';
import { QuestionnaireModalComponent } from './components/details/questionnaire-modal/questionnaire-modal.component';

@NgModule({
  declarations: [
    DashboardColaboratorComponent,
    ProfileColaboratorComponent,
    DetailsColaboratorComponent,
    ListCardColaboratorComponent,
    EditColaboratorComponent,
    QuestionnaireModalComponent
  ],
  imports: [
    CommonModule,
    ColaboratorRoutingModule,
    SharedModule,
    MapModule,
    TableModule
  ],
  entryComponents: [
    QuestionnaireModalComponent
  ]
})
export class ColaboratorModule { }
