import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardColaboratorComponent } from './components/dashboard/dashboard.component';
import { ProfileColaboratorComponent } from './components/profile/profile.component';
import { EditColaboratorComponent } from './components/edit/edit.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'dashboard',
        component: DashboardColaboratorComponent,
        data: {
          title: 'modules.colaborator.dashboard.title',
          parent: 'colaborator'
        }
      },
      {
        path: 'edit/:id',
        component: EditColaboratorComponent,
        data: {
          title: 'modules.colaborator.edit.title',
          parent: 'colaborator'
        }
      },
      {
        path: 'profile/:id',
        component: ProfileColaboratorComponent,
        data: {
          title: 'modules.colaborator.profile.title',
          parent: 'colaborator'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ColaboratorRoutingModule { }
