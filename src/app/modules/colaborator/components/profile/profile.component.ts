import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, Input, Output } from '@angular/core';
import { MapLongitudeCenter, MapLatitudeCenter, MapZoomNeighborhood } from 'src/app/helper/config';
import { MapFilter } from 'src/app/models/map-filter';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Colaborator } from 'src/app/models/colaborator';
import { getClassByStatus, isNullOrEmptyOrUndefined } from 'src/app/helper/utils';
import { UserService } from 'src/app/core/services/user/user.service';
import { CatalogApiScopes } from 'src/app/helper/catalog';
import { BaseResponse } from 'src/app/models/base-response';
import { Position } from 'src/app/models/position';
import { getMarker, getRoute } from 'src/app/helper/map';

@Component({
  selector: 'app-profile-colaborator',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProfileColaboratorComponent implements OnInit {
  statusClass: string;
  colaborator: Colaborator;
  status: string;
  id: number;
  routeTitle: string;
  filterForm: FormGroup;
  mapLat = MapLatitudeCenter;
  mapLng = MapLongitudeCenter;
  mapZoom = MapZoomNeighborhood;
  filters: object = {
    interaction: false,
    routes: false,
    inSafehouse: false
  };
  filtersMap: MapFilter[] = [
    {
      key: 'interaction',
      value: false,
      type: 'checkbox',
      name: 'Colaboradores con posible interacción'
    },
    {
      key: 'routes',
      value: false,
      type: 'checkbox',
      name: 'Ver recorrido'
    },
    {
      key: 'inSafehouse',
      value: false,
      type: 'checkbox',
      name: 'En domicilio'
    }
  ];
  routes = [];
  markers = [];
  matchs = [];
  matchUsersIds = [];
  colaboratorPosition;

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private userServices: UserService,
    private cdr: ChangeDetectorRef,
  ) {
    this.filterForm = this.fb.group({
      from: [''],
      to: ['']
    });
  }

  ngOnInit() {
    this.id = +this.route.snapshot.paramMap.get('id');
    this.routeTitle = this.route.snapshot.data.title;
    this.getProfile();
  }

  onSubmitFilterForm() {
    this.getPositions();
  }

  getProfile() {
    const filters = {
      scope: `${CatalogApiScopes.latestPosition},${CatalogApiScopes.city},${CatalogApiScopes.neighborhood}`
    };
    this.userServices.getOne<Colaborator>(this.id, filters).subscribe( response => {
      this.colaborator = new Colaborator(response.data);
      this.statusClass = getClassByStatus(this.colaborator.status);
      // Set latest position, if is not defined set safehouse
      this.colaboratorPosition = !isNullOrEmptyOrUndefined(this.colaborator.latestPosition) ? this.colaborator.latestPosition.position : this.colaborator.safehouse;
      this.mapLat = this.colaboratorPosition.coordinates[0];
      this.mapLng = this.colaboratorPosition.coordinates[1];
      this.markers.push(getMarker(this.colaborator, this.colaboratorPosition));
      // Get positions
      this.getPositions();
      this.cdr.detectChanges();
    });
  }

  getPositions() {
    this.routes = [];
    this.matchs = [];
    const filters = {
      idUser: this.id,
      scope: `${CatalogApiScopes.positionsWithMatch}`
    };
    if (!isNullOrEmptyOrUndefined(this.filterForm.value.from)) {
      filters['from'] = this.filterForm.value.from;
    }
    if (!isNullOrEmptyOrUndefined(this.filterForm.value.to)) {
      filters['to'] = this.filterForm.value.to;
    }
    this.userServices.position(filters).subscribe( response => {
      const positions = new BaseResponse<Position>(response, Position);
      // Get positions
      positions['data']['rows'].forEach( position => {
        // Get matchs
        if (position.match) {
          position.match.forEach( match => {
            // Update user status with the status at the match moment
            match.user.status = match.status;
            // Don't repeat match with the same user
            if (this.matchUsersIds.indexOf(match.user.id) === -1) {
              this.matchUsersIds.push(match.user.id);
              this.matchs.push({
                ...getMarker(match.user, position.position),
                outOfSafehouse: match.outOfSafehouse
              });
            }
          });
        }
      });
      this.getFilteredMarkers();
    });
  }

  changeMapFilters(event) {
    this.filters = event;
    this.getFilteredMarkers();
  }

  getFilteredMarkers() {
    this.markers = [];
    this.routes = [];
    if (this.filters['routes']) {
      // Routes points = points of matchs
      this.routes = this.matchs;
    } else {
      // Set actual position
      this.markers.push(getMarker(this.colaborator, this.colaboratorPosition));
      const matchs = (this.filters['inSafehouse']) ? this.matchs.filter(item => item.outOfSafehouse === false) : this.matchs.filter(item => item.outOfSafehouse === true);
      if (this.filters['interaction']) {
        this.markers = [
          ...this.markers,
          ...matchs
        ];
      }
    }
    this.cdr.detectChanges();
  }
}
