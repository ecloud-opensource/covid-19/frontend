import { Component, OnInit, ChangeDetectionStrategy, Input, OnChanges, Output, EventEmitter, ChangeDetectorRef } from '@angular/core';
import { MapLatitudeCenter, MapLongitudeCenter, MapZoom, MapZoomNeighborhood } from 'src/app/helper/config';
import { Colaborator } from 'src/app/models/colaborator';
import { ToastService } from 'src/app/core/services/toast/toast.service';
import { getClassByStatus } from '../../../../helper/utils';
import { getMarker } from 'src/app/helper/map';
import { SweetAlertService } from 'src/app/core/services/sweet-alert/sweet-alert.service';
import { UserService } from 'src/app/core/services/user/user.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Question } from 'src/app/models/question';
import { BaseResponse } from 'src/app/models/base-response';
import { QuestionnaireModalComponent } from './questionnaire-modal/questionnaire-modal.component';
import { CatalogQuestionnaireTopics } from 'src/app/helper/catalog';

@Component({
  selector: 'app-details-colaborator',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DetailsColaboratorComponent implements OnInit, OnChanges {
  mapLat = MapLatitudeCenter;
  mapLng = MapLongitudeCenter;
  mapZoom = MapZoom;
  routeTitle: string;
  parent: string;
  markers: object[] = [];
  statusClass: string;
  questionnaire: Question[];
  questions = [];
  catalogQuestionnaireTopics = CatalogQuestionnaireTopics;

  @Input() colaborator: Colaborator;

  @Output() updateQuarantined = new EventEmitter();

  constructor(
    private cdr: ChangeDetectorRef,
    public toast: ToastService,
    private sweetAlertService: SweetAlertService,
    private userService: UserService,
    private modalService: NgbModal
  ) { }

  ngOnInit() {
  }

  ngOnChanges() {
    if (this.colaborator) {
      this.getQuestionnaire();
      if (this.colaborator.safehouse) {
        this.markers = [];
        this.mapLat = this.colaborator.safehouse.coordinates[0];
        this.mapLng = this.colaborator.safehouse.coordinates[1];
        this.mapZoom = MapZoomNeighborhood;
        this.markers.push(getMarker(this.colaborator, this.colaborator.safehouse));
      }
      this.statusClass = getClassByStatus(this.colaborator.status);
      this.cdr.detectChanges();
    }
  }

  getQuestionnaire() {
    this.userService.getQuestionnaire(this.colaborator.id).subscribe( response => {
      this.questionnaire = new BaseResponse<Question>(response, Question).data['rows'];
      this.catalogQuestionnaireTopics.forEach( questionnaireTopic => {
        const temp = this.questionnaire.find( question => question.question === questionnaireTopic.key);
        if (questionnaireTopic.isTitle) {
          this.questions.push(questionnaireTopic);
        } else if (temp) {
          questionnaireTopic['value'] = temp.answer === '' ? '-' : temp.answer;
          this.questions.push(questionnaireTopic);
        }
      });
    });
  }

  toggleMeasure() {
    const confirmMessage = this.colaborator.isQuarantined ? 'modules.colaborator.details.confirmRemoveEstablishedMeasure' : 'modules.colaborator.details.confirmApplyEstablishedMeasure';
    this.sweetAlertService.confirmAction(confirmMessage).then(result => {
      if (result.value) {
        const params = {
          isQuarantined: !this.colaborator.isQuarantined
        };
        this.userService.edit(params, this.colaborator.id).subscribe(
          _ => {
            this.updateQuarantined.emit();
            this.toast.success('modules.colaborator.details.measureUpdated');
          }
        );
      }
    });
  }

  openQuestionnaire() {
    const modalRef = this.modalService.open(QuestionnaireModalComponent, {size: 'xl', scrollable: true});
    modalRef.componentInstance.questionnaire = this.questions;
  }
}

