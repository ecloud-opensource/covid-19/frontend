import { Component, Input } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-questionnaire-modal',
  templateUrl: './questionnaire-modal.component.html',
  styles: [ './questionnaire-modal.component.scss']
})
export class QuestionnaireModalComponent {
  @Input() questionnaire: any[] = [];

  constructor(private modalService: NgbModal) {
  }

  close() {
    this.modalService.dismissAll();
  }
}
