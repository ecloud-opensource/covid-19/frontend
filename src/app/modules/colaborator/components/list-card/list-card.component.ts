import { Component, OnInit, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';
import { Colaborator } from 'src/app/models/colaborator';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-list-card-colaborator',
  templateUrl: './list-card.component.html',
  styleUrls: ['./list-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ListCardColaboratorComponent implements OnInit {
  routeTitle: string;
  parent: string;

  @Input() records: Colaborator[];
  @Input() perPage: number;
  @Input() totalRecords: number;
  @Input() pageNumber: number;

  @Output() changePage = new EventEmitter();

  constructor(
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    this.routeTitle = this.route.snapshot.data['title'];
    this.parent = this.route.snapshot.data['parent'];
  }

  onChangePage(event) {
    this.changePage.emit(event);
  }

  view(id: number) {
    this.router.navigate([this.parent, 'profile', id]);
  }

}
