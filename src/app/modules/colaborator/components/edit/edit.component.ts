import { Component, OnInit, ChangeDetectionStrategy, EventEmitter, Output, ChangeDetectorRef } from '@angular/core';
import { Observable } from 'rxjs';
import { Colaborator } from 'src/app/models/colaborator';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastService } from 'src/app/core/services/toast/toast.service';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { UserService } from 'src/app/core/services/user/user.service';

@Component({
  selector: 'app-edit-colaborator',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditColaboratorComponent implements OnInit {
  id: number;
  routeTitle: string;
  parent: string;
  colaboratorForm: FormGroup;

  @Output() sendFormData = new EventEmitter();

  constructor(
    private cdr: ChangeDetectorRef,
    private router: Router,
    private route: ActivatedRoute,
    public toast: ToastService,
    private userService: UserService,
    private fb: FormBuilder
  ) {
    this.parent = this.route.snapshot.data.parent;
    this.colaboratorForm = this.fb.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      phone: ['', Validators.required],
      address: ['', Validators.required]
    });
  }

  ngOnInit() {
    this.id = +this.route.snapshot.paramMap.get('id');
    this.routeTitle = this.route.snapshot.data.title;
    this.getColaborator();
  }

  getColaborator() {
    this.userService.getOne<Colaborator>(this.id).subscribe( response => {
      const colaborator = new Colaborator(response.data);
      this.colaboratorForm.patchValue(colaborator);
      this.cdr.detectChanges();
    });
  }

  onSubmit() {
    if (this.colaboratorForm.valid) {
      this.userService.edit<Colaborator>(this.colaboratorForm.value, this.id).subscribe( _ => {
        this.toast.success('modules.colaborator.edit.edited');
        this.goBack();
      });
    }
  }

  goBack() {
    this.router.navigate([this.parent, 'dashboard']);
  }

}
