import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MapLatitudeCenter, MapLongitudeCenter, MapZoom } from 'src/app/helper/config';
import { MapFilter } from 'src/app/models/map-filter';
import { FormGroupFilters } from 'src/app/shared/modules/table/models/table-filter.model';
import { ColumnSetting } from 'src/app/shared/modules/table/models/table-layout.model';
import { BaseResponse } from 'src/app/models/base-response';
import { Colaborator } from 'src/app/models/colaborator';
import { UserService } from 'src/app/core/services/user/user.service';
import { EndpointsConstants } from 'src/app/helper/endpoints';
import { ActivatedRoute, Router } from '@angular/router';
import { generateOptionsByObject, isNull } from 'src/app/helper/utils';
import { CatalogUserStatuses } from 'src/app/helper/catalog';
import { GeoService } from 'src/app/core/services/geo/geo.service';
import { getMarker } from 'src/app/helper/map';

@Component({
  selector: 'app-dashboard-colaborator',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DashboardColaboratorComponent implements OnInit {
  catalogUserStatuses = generateOptionsByObject(CatalogUserStatuses, 'key', 'label');
  filterMapForm: FormGroup;
  mapLat = MapLatitudeCenter;
  mapLng = MapLongitudeCenter;
  mapZoom = MapZoom;
  resultFiltersMap: object = {
    heatmap: false,
    onMovement: true,
    inHome: true
  };
  filtersMap: MapFilter[] = [
    {
      key: 'heatmap',
      value: false,
      type: 'checkbox',
      name: 'Concentración de afectados'
    },
    {
      key: 'onMovement',
      value: true,
      type: 'checkbox',
      name: 'En movimiento'
    },
    {
      key: 'inHome',
      value: true,
      type: 'checkbox',
      name: 'En domicilio'
    }
  ];
  tableActions = ['view', 'edit'];
  tableFilters: FormGroupFilters[] = [
    {
      name: 'term',
      placeholder: 'forms.fields.searchByNameIdOrAddress',
      type: 'input',
      defaultValue: ''
    },
    {
      name: 'status',
      type: 'select-search-simple',
      options: generateOptionsByObject(CatalogUserStatuses, 'key', 'label'),
      placeholder: 'forms.fields.userClasification',
      valueBind: 'value',
      labelBind: 'label',
      defaultValue: null
    }
  ];
  columnSettings: ColumnSetting[] = [
    {
      primaryKey: 'id',
      header: 'modules.colaborator.dashboard.id'
    },
    {
      primaryKey: 'fullName',
      header: 'modules.colaborator.dashboard.name'
    },
    {
      primaryKey: 'identification',
      header: 'modules.colaborator.dashboard.identification'
    },
    {
      primaryKey: 'phone',
      header: 'modules.colaborator.dashboard.phone'
    },
    {
      primaryKey: 'email',
      header: 'modules.colaborator.dashboard.email'
    }
  ];
  filters: any[] = [];
  pageNumber = 1;
  results: BaseResponse<Colaborator>;
  routeTitle: string;
  parent: string;
  // Map settings
  showLoader = false;
  heatmap: object[] = [];
  markers: object[] = [];
  bounds;
  mapData: object[] = [];

  constructor(
    private cdr: ChangeDetectorRef,
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private userService: UserService,
    private geoService: GeoService
  ) {
    this.filterMapForm = this.fb.group({
      status: [null]
    });
  }

  ngOnInit() {
    this.routeTitle = this.route.snapshot.data['title'];
    this.parent = this.route.snapshot.data['parent'];
    this.updateList(1);
  }

  updateList(page: number) {
    this.pageNumber = page;
    this.userService.get<Colaborator>(page, EndpointsConstants.requestLimit, this.filters).subscribe( response => {
      this.results = new BaseResponse<Colaborator>(response, Colaborator);
      this.cdr.detectChanges();
    });
  }

  goToProfile(id: number) {
    this.router.navigate([this.parent, 'profile', id]);
  }

  edit(id: number) {
    this.router.navigate([this.parent, 'edit', id]);
  }

  onSubmitFilters(event) {
    this.filters = event;
    this.updateList(1);
  }

  onSubmitFilterMapForm() {
    this.getGeoService();
  }

  changeMapFilters(event) {
    this.resultFiltersMap = event;
    this.getMapData();
  }

  onBoundsChange(bounds) {
    this.bounds = bounds;
    this.getGeoService();
  }

  getGeoService() {
    this.changeLoaderVisibility(true);
    const params = {
      ...this.bounds
    };
    const status = this.filterMapForm.get('status').value;
    if (!isNull(status)) {
      params['status'] = status;
    }
    this.geoService.getAdminNearBy(params).subscribe( response => {
      this.mapData = response.data['users'];
      this.getMapData();
      this.changeLoaderVisibility(false);
    });
  }

  getMapData() {
    this.getMarkers();
    this.getHeatmap();
    this.cdr.detectChanges();
  }

  getMarkers() {
    this.markers = [];
    // Filter on movement
    const filterOnMovement = this.mapData.filter( item => item['onMovement'] === this.resultFiltersMap['onMovement']);
    // Filter in home
    const filterInHome = this.mapData.filter( item => item['outOfSafehouse'] !== this.resultFiltersMap['inHome']);
    // Concat filters
    const data = filterOnMovement.concat(filterInHome);
    data.forEach( user => {
      // Avoid repeated markers
      const isMarkerAddedPreviously = this.markers.find( item => item['latitude'] === user['latestPosition'].position.coordinates[0] && item['longitude'] === user['latestPosition'].position.coordinates[1]);
      if (!isNull(user['status']) && !isMarkerAddedPreviously) {
        this.markers.push(getMarker(user, user['latestPosition'].position));
      }
    });
  }

  getHeatmap() {
    if (!this.resultFiltersMap['heatmap']) {
      this.heatmap = [];
    } else {
      const data = [];
      this.mapData.forEach( user => {
        data.push(new google.maps.LatLng(user['latestPosition'].position.coordinates[0], user['latestPosition'].position.coordinates[1]));
      });
      this.heatmap = data;
    }
  }

  changeLoaderVisibility(isVisible) {
    this.showLoader = isVisible;
    this.cdr.detectChanges();
  }
}
