import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { ColumnSetting } from 'src/app/shared/modules/table/models/table-layout.model';
import { MapLatitudeCenter, MapLongitudeCenter, MapZoom, MapZoomCountry, MapZoomCity, MapZoomNeighborhood } from 'src/app/helper/config';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Country } from 'src/app/models/country';
import { City } from 'src/app/models/city';
import { CountryService } from 'src/app/core/services/country/country.service';
import { CityService } from 'src/app/core/services/city/city.service';
import { BaseResponse } from 'src/app/models/base-response';
import { isUndefined, isNull, isNullOrEmptyOrUndefined } from 'src/app/helper/utils';
import { EndpointsConstants } from 'src/app/helper/endpoints';
import { GeoService } from 'src/app/core/services/geo/geo.service';
import { getMarker } from 'src/app/helper/map';
import { Neighborhood } from 'src/app/models/neighborhood';
import { NeighborhoodService } from 'src/app/core/services/neighborhood/neighborhood.service';
import { CatalogLogFields } from 'src/app/helper/catalog';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalAddCaseComponent } from 'src/app/shared/components/modal-add-case/modal-add-case.component';
import { ModalRemoveCaseComponent } from 'src/app/shared/components/modal-remove-case/modal-remove-case.component';
import { Log } from 'src/app/models/log';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DashboardCaseRecordComponent implements OnInit {
  filterForm: FormGroup;
  mapLat = MapLatitudeCenter;
  mapLng = MapLongitudeCenter;
  mapZoom = MapZoom;
  tableActions = [];
  columnSettings: ColumnSetting[] = [
    {
      primaryKey: 'name',
      header: 'modules.caseRecord.dashboard.name'
    },
    {
      primaryKey: 'fellows',
      header: 'modules.caseRecord.dashboard.numberOfCollaborators'
    },
    {
      primaryKey: 'areInfected',
      header: 'modules.caseRecord.dashboard.numberOfCase'
    }
  ];
  filters: any[] = [];
  pageNumber = 1;
  listResults: BaseResponse<Country | City | Neighborhood>;
  countries: Country[] = [];
  neighborhoods: Neighborhood[] = [];
  cities: City[] = [];
  logs: Log[] = [];
  showLoader = false;
  markers: object[] = [];
  polygons: object[] = [];
  bounds;
  logLocationName: string;
  tableTitle = 'modules.caseRecord.dashboard.registerOfCountries';

  constructor(
    private cdr: ChangeDetectorRef,
    private fb: FormBuilder,
    private modalService: NgbModal,
    private countryService: CountryService,
    private cityService: CityService,
    private neighborhoodService: NeighborhoodService,
    private geoService: GeoService,
    private translateService: TranslateService
  ) {
    this.filterForm = this.fb.group({
      idCountry: [null],
      idCity: [null],
      idNeighborhood: [null]
    });
  }

  ngOnInit() {
    this.updateList(1);
    this.getCountriesForFilters();
  }

  updateList(page: number) {
    this.pageNumber = page;
    const formFilterModel = this.filterForm.value;
    if (!isNullOrEmptyOrUndefined(formFilterModel.idCity) || !isNullOrEmptyOrUndefined(formFilterModel.idNeighborhood)) {
      this.getNeighborhoodsForList(page);
    } else if (!isNullOrEmptyOrUndefined(formFilterModel.idCountry)) {
      this.getCitiesForList(page);
    } else {
      this.getCountriesForList(page);
    }
  }

  getCountriesForList(page) {
    this.tableTitle = 'modules.caseRecord.dashboard.registerOfCountries';
    this.countryService.get<Country>(page, EndpointsConstants.requestLimit).subscribe( response => {
      this.listResults = new BaseResponse<Country>(response, Country);
      this.cdr.detectChanges();
    });
  }

  getCitiesForList(page) {
    const idCountry = this.filterForm.value.idCountry;
    const country = this.countries.find( item => item.id === idCountry);
    this.tableTitle = this.translateService.instant('modules.caseRecord.dashboard.registerOf') + ' ' + country.name;
    const filters = {
      idCountry
    };
    this.cityService.get<City>(page, EndpointsConstants.requestLimit, filters).subscribe( response => {
      this.listResults = new BaseResponse<City>(response, City);
      this.cdr.detectChanges();
    });
  }

  getNeighborhoodsForList(page) {
    const idCity = this.filterForm.value.idCity;
    const idNeighborhood = this.filterForm.value.idNeighborhood;
    const city = this.cities.find( item => item.id === idCity);
    this.tableTitle = this.translateService.instant('modules.caseRecord.dashboard.registerOf') + ' ' + city.name;
    const filters = {
      idCity
    };
    if (!isNullOrEmptyOrUndefined(idNeighborhood)) {
      filters['id'] = idNeighborhood;
    }
    this.neighborhoodService.get<Neighborhood>(page, EndpointsConstants.requestLimit, filters).subscribe( response => {
      this.listResults = new BaseResponse<Neighborhood>(response, Neighborhood);
      this.cdr.detectChanges();
    });
  }

  onCountryChange(country) {
    if (!isUndefined(country)) {
      this.getCitiesForFilters(country.id);
    } else {
      this.cities = [];
      this.cdr.detectChanges();
    }
  }

  onCityChange(city) {
    if (!isUndefined(city)) {
      this.getNeighborhoodsForFilters(city.id);
    } else {
      this.cities = [];
      this.cdr.detectChanges();
    }
  }

  getCountriesForFilters(term?) {
    const filters = {
      term: term ? term : ''
    };
    this.countryService.get<Country>(1, EndpointsConstants.requestLimit, filters).subscribe(response => {
      this.countries = new BaseResponse<Country>(response, Country).data['rows'];
      this.cdr.detectChanges();
    });
  }

  getCitiesForFilters(idCountry, term?) {
    const filters = {
      idCountry,
      term: term ? term : ''
    };
    this.cityService.get<City>(1, EndpointsConstants.requestLimit, filters).subscribe(response => {
      this.cities = new BaseResponse<City>(response, City).data['rows'];
      this.cdr.detectChanges();
    });
  }

  getNeighborhoodsForFilters(idCity, term?) {
    const filters = {
      idCity,
      term: term ? term : ''
    };
    this.neighborhoodService.get<Neighborhood>(1, EndpointsConstants.requestLimit, filters).subscribe(response => {
      this.neighborhoods = new BaseResponse<Neighborhood>(response, Neighborhood).data['rows'];
      this.cdr.detectChanges();
    });
  }

  applyFilters() {
    this.updateList(1);
    this.getLogs();
    this.changeMapBounds();
  }

  changeMapBounds() {
    const idCountry = this.filterForm.value.idCountry;
    const idCity = this.filterForm.value.idCity;
    const idNeighborhood = this.filterForm.value.idNeighborhood;
    let location;
    let zoom;
    // Find neighborhood (priority)
    if (!isNull(idNeighborhood)) {
      location = this.neighborhoods.find( item => item.id === idNeighborhood);
      zoom = MapZoomNeighborhood;
      this.drawNeighborhood(true);
    } else {
      this.drawNeighborhood(false);
    }
    // Find city (second priority)
    if (isUndefined(location) && !isNull(idCity)) {
      location = this.cities.find( item => item.id === idCity);
      zoom = MapZoomCity;
    }
    // Find country
    if (isUndefined(location) && !isNull(idCountry)) {
      location = this.countries.find( item => item.id === idCountry);
      zoom = MapZoomCountry;
    }
    // Replace map location if it is necessary
    this.mapZoom = !isUndefined(zoom) ? zoom : MapZoom;
    this.mapLat = !isUndefined(location) ? location.center.coordinates[0] : MapLatitudeCenter;
    this.mapLng = !isUndefined(location) ? location.center.coordinates[1] : MapLongitudeCenter;
    this.cdr.detectChanges();
  }

  drawNeighborhood(addPolygon: boolean) {
    // Remove last polygon
    this.polygons = [];
    if (addPolygon) {
      const neighborhood = this.neighborhoods.find( item => item.id === this.filterForm.value.idNeighborhood);
      if (neighborhood) {
        const polygon = [];
        neighborhood.area.coordinates[0].forEach( coordinate => {
          polygon.push({
            lat: +coordinate[0],
            lng: +coordinate[1]
          });
        });
        this.polygons.push(polygon);
      }
    }
    this.cdr.detectChanges();
  }

  onBoundsChange(bounds) {
    this.bounds = bounds;
    this.getGeoService();
  }

  getGeoService() {
    this.changeLoaderVisibility(true);
    this.geoService.getAdminNearBy(this.bounds).subscribe( response => {
      response.data['users'].forEach( user => {
        // Avoid repeated markers
        const isMarkerAddedPreviously = this.markers.find( item => item['latitude'] === user['latestPosition'].position.coordinates[0] && item['longitude'] === user['latestPosition'].position.coordinates[1]);
        if (!isNull(user['status']) && !isMarkerAddedPreviously) {
          this.markers.push(getMarker(user, user.latestPosition.position));
        }
      });
      this.changeLoaderVisibility(false);
    });
  }

  changeLoaderVisibility(isVisible) {
    this.showLoader = isVisible;
    this.cdr.detectChanges();
  }

  getLogs() {
    const idCity = this.filterForm.value.idCity;
    const idNeighborhood = this.filterForm.value.idNeighborhood;
    this.logs = [];
    if (!isNull(idNeighborhood)) {
      this.getNeighborhoodLogs(idNeighborhood);
    } else if (!isNull(idCity)) {
      this.getCityLogs(idCity);
    }
  }

  getNeighborhoodLogs(idNeighborhood) {
    const neighborhood = this.neighborhoods.find( item => item.id === this.filterForm.value.idNeighborhood);
    this.logLocationName = neighborhood.name;
    const filters = {
      field: CatalogLogFields.areInfected,
      idNeighborhood
    };
    this.neighborhoodService.log(filters).subscribe( response => {
      this.logs = response.data['rows'];
      this.cdr.detectChanges();
    });
  }

  getCityLogs(idCity) {
    const city = this.cities.find( item => item.id === this.filterForm.value.idCity);
    this.logLocationName = city.name;
    const filters = {
      field: CatalogLogFields.areInfected,
      idCity
    };
    this.cityService.log(filters).subscribe( response => {
      this.logs = response.data['rows'];
      this.cdr.detectChanges();
    });
  }

  addCase() {
    const modalRef = this.modalService.open(ModalAddCaseComponent);
    modalRef.result.then(
      () => { this.updateList(1); },
      () => { this.updateList(1); }
    );
  }

  removeCase() {
    const modalRef = this.modalService.open(ModalRemoveCaseComponent);
    modalRef.result.then(
      () => { this.updateList(1); },
      () => { this.updateList(1); }
    );
  }
}
