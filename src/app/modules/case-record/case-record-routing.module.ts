import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardCaseRecordComponent } from './components/dashboard/dashboard.component';


const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'dashboard',
        component: DashboardCaseRecordComponent,
        data: {
          title: 'modules.caseRecord.dashboard.title',
          parent: 'case-record'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CaseRecordRoutingModule { }
