import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { CaseRecordRoutingModule } from './case-record-routing.module';
import { TableModule } from 'src/app/shared/modules/table/table.module';
import { MapModule } from 'src/app/shared/modules/map/map.module';
import { DashboardCaseRecordComponent } from './components/dashboard/dashboard.component';
import { ModalAddCaseComponent } from 'src/app/shared/components/modal-add-case/modal-add-case.component';
import { ModalRemoveCaseComponent } from 'src/app/shared/components/modal-remove-case/modal-remove-case.component';

@NgModule({
  declarations: [
    DashboardCaseRecordComponent
  ],
  imports: [
    CommonModule,
    CaseRecordRoutingModule,
    TableModule,
    SharedModule,
    MapModule
  ],
  entryComponents: [
    ModalAddCaseComponent,
    ModalRemoveCaseComponent
  ]
})
export class CaseRecordModule { }
