import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { FormGroupFilters } from 'src/app/shared/modules/table/models/table-filter.model';
import { ColumnSetting } from 'src/app/shared/modules/table/models/table-layout.model';
import { BaseResponse } from 'src/app/models/base-response';
import { State } from 'src/app/models/state';
import { StateService } from 'src/app/core/services/state/state.service';
import { Router } from '@angular/router';
import { EndpointsConstants } from 'src/app/helper/endpoints';
import { CatalogApiScopes } from 'src/app/helper/catalog';

@Component({
  selector: 'app-list-state',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ListStateComponent implements OnInit {
  tableActions = ['edit'];
  tableFilters: FormGroupFilters[] = [
    {
      name: 'term',
      placeholder: 'forms.fields.searchByName',
      type: 'input',
      defaultValue: ''
    }
  ];
  columnSettings: ColumnSetting[] = [
    {
      primaryKey: 'id',
      header: 'modules.state.list.id'
    },
    {
      primaryKey: 'name',
      header: 'modules.state.list.name'
    },
    {
      primaryKey: 'country.name',
      header: 'modules.state.list.country'
    }
  ];
  filters: any[] = [];
  pageNumber = 1;
  results: BaseResponse<State>;

  constructor(
    private cdr: ChangeDetectorRef,
    private router: Router,
    private stateService: StateService
  ) { }

  ngOnInit() {
    this.updateList(1);
  }

  updateList(page: number) {
    this.pageNumber = page;
    const filters = {
      ...this.filters,
      scope: CatalogApiScopes.country
    };
    this.stateService.get<State>(page, EndpointsConstants.requestLimit, filters).subscribe( response => {
      this.results = new BaseResponse<State>(response, State);
      this.cdr.detectChanges();
    });
  }

  edit(id: number) {
    this.router.navigate(['state', 'edit', id]);
  }

  clickFilters(event) {
    this.filters = event;
    this.updateList(1);
  }

}
