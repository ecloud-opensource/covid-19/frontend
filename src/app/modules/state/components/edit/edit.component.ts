import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Observable } from 'rxjs';
import { State } from 'src/app/models/state';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastService } from 'src/app/core/services/toast/toast.service';
import { StateService } from 'src/app/core/services/state/state.service';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-edit-state',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditStateComponent implements OnInit {
  id: number;
  currentState$: Observable<State>;
  parent: string;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    public toast: ToastService,
    private stateService: StateService
  ) {
    this.parent = this.route.snapshot.data.parent;
  }

  ngOnInit() {
    this.id = +this.route.snapshot.paramMap.get('id');
    this.getState();
  }

  getState() {
    this.currentState$ = this.stateService.getOne<State>(this.id)
    .pipe (
      map( response => {
        return new State(response.data);
      })
    );
  }

  getFormData(currentState: State) {
    this.stateService.edit<State>(currentState, this.id).subscribe( _ => {
      this.toast.success('modules.state.edit.edited');
      this.goBack();
    });
  }

  goBack() {
    this.router.navigate([this.parent, 'list']);
  }

}
