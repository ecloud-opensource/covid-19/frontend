import { Component, OnInit, ChangeDetectionStrategy, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { State } from 'src/app/models/state';
import { Router, ActivatedRoute } from '@angular/router';
import { MapZoomState } from 'src/app/helper/config';

@Component({
  selector: 'app-form-state',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FormStateComponent implements OnInit, OnChanges {
  routeTitle: string;
  parent: string;
  stateForm: FormGroup;
  zoom = MapZoomState;
  marker;

  @Input() currentState: State;

  @Output() sendFormData = new EventEmitter();

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder
  ) {
    this.parent = this.route.snapshot.data.parent;
    this.stateForm = this.fb.group({
      name: ['', Validators.required],
      code: ['', Validators.required],
      center: ['', Validators.required],
      area: ['', Validators.required],
      idCountry: ['', Validators.required]
    });
  }

  ngOnInit() {
    this.routeTitle = this.route.snapshot.data.title;
  }

  ngOnChanges() {
    if (this.currentState) {
      this.marker = {
        latitude: this.currentState.center.coordinates[0],
        longitude: this.currentState.center.coordinates[1]
      };
      this.stateForm.patchValue(this.currentState);
    }
  }

  onSubmit() {
    if (this.stateForm.valid) {
      this.sendFormData.emit(this.stateForm.value);
    }
  }

  goBack() {
    this.router.navigate([this.parent, 'list']);
  }

}
