import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StateRoutingModule } from './state-routing.module';
import { EditStateComponent } from './components/edit/edit.component';
import { FormStateComponent } from './components/form/form.component';
import { ListStateComponent } from './components/list/list.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { TableModule } from 'src/app/shared/modules/table/table.module';
import { MapModule } from 'src/app/shared/modules/map/map.module';

@NgModule({
  declarations: [
    EditStateComponent,
    FormStateComponent,
    ListStateComponent
  ],
  imports: [
    CommonModule,
    StateRoutingModule,
    SharedModule,
    TableModule,
    MapModule
  ]
})
export class StateModule { }
