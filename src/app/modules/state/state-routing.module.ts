import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListStateComponent } from './components/list/list.component';
import { EditStateComponent } from './components/edit/edit.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'list',
        component: ListStateComponent,
        data: {
          title: 'modules.state.list.title',
          parent: 'state'
        }
      },
      {
        path: 'edit/:id',
        component: EditStateComponent,
        data: {
          title: 'modules.state.edit.title',
          parent: 'state'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StateRoutingModule { }
