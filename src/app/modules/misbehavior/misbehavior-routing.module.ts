import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardMisbehaviorComponent } from './components/dashboard/dashboard/dashboard.component';


const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'dashboard',
        component: DashboardMisbehaviorComponent,
        data: {
          title: 'modules.misbehavior.dashboard.title',
          parent: 'misbehavior'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MisbehaviorRoutingModule { }
