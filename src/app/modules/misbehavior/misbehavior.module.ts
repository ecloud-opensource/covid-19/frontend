import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MisbehaviorRoutingModule } from './misbehavior-routing.module';
import { DashboardMisbehaviorComponent } from './components/dashboard/dashboard/dashboard.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { TableModule } from 'src/app/shared/modules/table/table.module';
import { MisbehaviorModalComponent } from './components/misbehavior-modal/misbehavior-modal.component';


@NgModule({
  declarations: [
    DashboardMisbehaviorComponent,
    MisbehaviorModalComponent
  ],
  imports: [
    CommonModule,
    MisbehaviorRoutingModule,
    SharedModule,
    TableModule,
  ],
  entryComponents: [
    MisbehaviorModalComponent
  ]
})
export class MisbehaviorModule { }
