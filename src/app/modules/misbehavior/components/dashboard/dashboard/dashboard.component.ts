import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Validators } from '@angular/forms';
import { ColumnSetting } from 'src/app/shared/modules/table/models/table-layout.model';
import { FormGroupFilters } from 'src/app/shared/modules/table/models/table-filter.model';
import { NeighborhoodService } from 'src/app/core/services/neighborhood/neighborhood.service';
import { Neighborhood } from 'src/app/models/neighborhood';
import { BaseResponse } from 'src/app/models/base-response';
import { City } from 'src/app/models/city';
import { CityService } from 'src/app/core/services/city/city.service';
import { addFiltersTable } from 'src/app/helper/utils';
import { MisbehaviorService } from 'src/app/core/services/misbehavior/misbehavior.service';
import { Misbehavior } from 'src/app/models/misbehavior';
import { CatalogApiScopes } from 'src/app/helper/catalog';
import { EndpointsConstants } from 'src/app/helper/endpoints';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { MisbehaviorModalComponent } from '../../misbehavior-modal/misbehavior-modal.component';

@Component({
  selector: 'app-dashboard-misbehavior',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DashboardMisbehaviorComponent implements OnInit {
  routeTitle: string;
  misbehaviors: BaseResponse<Misbehavior>;
  quantity: number;
  neighborhoods: BaseResponse<Neighborhood>;
  cities: BaseResponse<City>;
  tableFilters: FormGroupFilters[] = [
    {
      name: 'date',
      type: 'input-date-range',
      placeholder: 'forms.fields.date'
    },
    {
      name: 'city',
      type: 'select-search-simple'
    },
    {
      name: 'neighborhood',
      type: 'select-search-simple'
    }
  ];
  tableActions = ['view'];
  columnSettings: ColumnSetting[] = [
    {
      primaryKey: 'id',
      header: 'modules.misbehavior.dashboard.numberOfMisbehavior'
    },
    {
      primaryKey: 'user.fullName',
      header: 'modules.misbehavior.dashboard.colaborator'
    },
    {
      primaryKey: 'from',
      header: 'modules.misbehavior.dashboard.dateFrom',
      isDate: true,
      dateFormat: 'EEE dd \'de\' LLL \'de\' y'
    },
    {
      primaryKey: 'to',
      header: 'modules.misbehavior.dashboard.dateTo',
      isDate: true,
      dateFormat: 'EEE dd \'de\' LLL \'de\' y'
    }
  ];
  filters: any[] = [];
  pageNumber = 1;
  results: any[] = [];

  constructor(
    private route: ActivatedRoute,
    private cityService: CityService,
    private neighborhoodService: NeighborhoodService,
    private misbehaviorService: MisbehaviorService,
    private modalService: NgbModal,
    private cdr: ChangeDetectorRef
  ) {}

  ngOnInit() {
    this.routeTitle = this.route.snapshot.data.title;
    this.getCities();
    this.getNeighborhoods(0);
    this.getMisbehaviors(1);
  }

  clickFilters(event) {
    if (event) {
      if (event['neighborhood']) {
        this.filters['idCity'] = event.city;
        this.filters['idNeighborhood'] = event.neighborhood;
        this.getMisbehaviors(1);
      }
      if (event['city']) {
        this.filters['idCity'] = event.city;
        this.getMisbehaviors(1);
      }
    }
  }

  dateRange(event) {
    this.filters['from'] = new Date(event.from);
    this.filters['to'] = new Date(event.to);
  }

  changeItems(event) {
    if (event.name === 'city') {
      this.getNeighborhoods(event.id);
    }
  }

  getCities() {
    this.cityService.get<City>(null, null, null).subscribe(response => {
      const results = new BaseResponse<City>(response, City);
      this.tableFilters = addFiltersTable(this.tableFilters, 'city', results.data['rows'], 'select-search-simple', 'forms.fields.city', 'name', 'id', null, [Validators.required]);
      this.cdr.detectChanges();
    });
  }

  getNeighborhoods(idCity) {
    const filters = {
        idCity
    };
    this.neighborhoodService.get<Neighborhood>(null, null, filters).subscribe(response => {
      const results = new BaseResponse<Neighborhood>(response, Neighborhood);
      this.tableFilters = addFiltersTable(this.tableFilters, 'neighborhood', results.data['rows'], 'select-search-simple', 'forms.fields.neighborhood', 'name', 'id', null, [Validators.required]);
      this.cdr.detectChanges();
    });
  }

  getMisbehaviors(page) {
    this.pageNumber = page;
    const filters = {
      ...this.filters,
      scope: `${CatalogApiScopes.user}`
    };
    this.misbehaviorService.get<Misbehavior>(this.pageNumber, EndpointsConstants.requestLimit, filters).subscribe(response => {
      this.misbehaviors = new BaseResponse<Misbehavior>(response, Misbehavior);
      this.cdr.detectChanges();
    });
  }

  view(event) {
    const modalRef = this.modalService.open(MisbehaviorModalComponent, {size: 'lg', centered: true});
    const misbehavior = this.misbehaviors.data['rows'].find( item => item.id === event);
    modalRef.componentInstance.misbehavior = new Misbehavior(misbehavior);
  }
}
