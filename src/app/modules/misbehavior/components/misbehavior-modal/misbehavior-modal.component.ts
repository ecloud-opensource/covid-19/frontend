import { Component, OnInit, ChangeDetectionStrategy, Input, ChangeDetectorRef } from '@angular/core';
import { Misbehavior } from 'src/app/models/misbehavior';
import { CityService } from 'src/app/core/services/city/city.service';
import { NeighborhoodService } from 'src/app/core/services/neighborhood/neighborhood.service';
import { City } from 'src/app/models/city';
import { Neighborhood } from 'src/app/models/neighborhood';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-misbehavior-modal',
  templateUrl: './misbehavior-modal.component.html',
  styleUrls: ['./misbehavior-modal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MisbehaviorModalComponent implements OnInit {
  @Input() misbehavior: Misbehavior;
  city: City;
  neighborhood: Neighborhood;

  constructor(
    public activeModal: NgbActiveModal,
    private cityService: CityService,
    private neighborhoodService: NeighborhoodService,
    private cdr: ChangeDetectorRef
  ) {
  }

  ngOnInit() {
    this.getCity();
    this.getNeighborhood();
  }

  getCity() {
    this.cityService.getOne<City>(this.misbehavior.idCity).subscribe(response => {
      this.city = new City(response.data);
      this.cdr.detectChanges();
    });
  }

  getNeighborhood() {
    this.neighborhoodService.getOne<Neighborhood>(this.misbehavior.idNeighborhood).subscribe(response => {
      this.neighborhood = new Neighborhood(response.data);
      this.cdr.detectChanges();
    });
  }

}
