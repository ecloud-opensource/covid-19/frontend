import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ComingSoonRoutingModule } from './coming-soon-routing.module';
import { ComingSoonComponent } from './components/coming-soon/coming-soon.component';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [
    ComingSoonComponent
  ],
  imports: [
    CommonModule,
    ComingSoonRoutingModule,
    SharedModule
  ]
})
export class ComingSoonModule { }
