import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Observable } from 'rxjs';
import { City } from 'src/app/models/city';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastService } from 'src/app/core/services/toast/toast.service';
import { CityService } from 'src/app/core/services/city/city.service';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-edit-city',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditCityComponent implements OnInit {
  id: number;
  currentCity$: Observable<City>;
  parent: string;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    public toast: ToastService,
    private cityService: CityService
  ) {
    this.parent = this.route.snapshot.data.parent;
  }

  ngOnInit() {
    this.id = +this.route.snapshot.paramMap.get('id');
    this.getCity();
  }

  getCity() {
    this.currentCity$ = this.cityService.getOne<City>(this.id)
    .pipe (
      map( response => {
        return new City(response.data);
      })
    );
  }

  getFormData(currentCity: City) {
    this.cityService.edit<City>(currentCity, this.id).subscribe( _ => {
      this.toast.success('modules.city.edit.edited');
      this.goBack();
    });
  }

  goBack() {
    this.router.navigate([this.parent, 'list']);
  }

}
