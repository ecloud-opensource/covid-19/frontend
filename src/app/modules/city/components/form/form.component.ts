import { Component, OnInit, ChangeDetectionStrategy, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { City } from 'src/app/models/city';
import { Router, ActivatedRoute } from '@angular/router';
import { MapZoomCity } from 'src/app/helper/config';

@Component({
  selector: 'app-form-city',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FormCityComponent implements OnInit, OnChanges {
  routeTitle: string;
  parent: string;
  cityForm: FormGroup;
  zoom = MapZoomCity;
  marker;

  @Input() currentCity: City;

  @Output() sendFormData = new EventEmitter();

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder
  ) {
    this.parent = this.route.snapshot.data.parent;
    this.cityForm = this.fb.group({
      name: ['', Validators.required],
      code: ['', Validators.required],
      center: ['', Validators.required],
      area: ['', Validators.required],
      idCountry: ['', Validators.required]
    });
  }

  ngOnInit() {
    this.routeTitle = this.route.snapshot.data.title;
  }

  ngOnChanges() {
    if (this.currentCity) {
      this.marker = {
        latitude: this.currentCity.center.coordinates[0],
        longitude: this.currentCity.center.coordinates[1]
      };
      this.cityForm.patchValue(this.currentCity);
    }
  }

  onSubmit() {
    if (this.cityForm.valid) {
      this.sendFormData.emit(this.cityForm.value);
    }
  }

  goBack() {
    this.router.navigate([this.parent, 'list']);
  }

}
