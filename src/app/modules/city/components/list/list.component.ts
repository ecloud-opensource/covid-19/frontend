import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { FormGroupFilters } from 'src/app/shared/modules/table/models/table-filter.model';
import { ColumnSetting } from 'src/app/shared/modules/table/models/table-layout.model';
import { BaseResponse } from 'src/app/models/base-response';
import { City } from 'src/app/models/city';
import { CityService } from 'src/app/core/services/city/city.service';
import { Router } from '@angular/router';
import { EndpointsConstants } from 'src/app/helper/endpoints';
import { CatalogApiScopes } from 'src/app/helper/catalog';

@Component({
  selector: 'app-list-city',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ListCityComponent implements OnInit {
  tableActions = ['edit'];
  tableFilters: FormGroupFilters[] = [
    {
      name: 'term',
      placeholder: 'forms.fields.searchByName',
      type: 'input',
      defaultValue: ''
    }
  ];
  columnSettings: ColumnSetting[] = [
    {
      primaryKey: 'id',
      header: 'modules.city.list.id'
    },
    {
      primaryKey: 'name',
      header: 'modules.city.list.name'
    },
    {
      primaryKey: 'country.name',
      header: 'modules.city.list.country'
    }
  ];
  filters: any[] = [];
  pageNumber = 1;
  results: BaseResponse<City>;

  constructor(
    private cdr: ChangeDetectorRef,
    private router: Router,
    private cityService: CityService
  ) { }

  ngOnInit() {
    this.updateList(1);
  }

  updateList(page: number) {
    this.pageNumber = page;
    const filters = {
      ...this.filters,
      scope: CatalogApiScopes.country
    };
    this.cityService.get<City>(page, EndpointsConstants.requestLimit, filters).subscribe( response => {
      this.results = new BaseResponse<City>(response, City);
      this.cdr.detectChanges();
    });
  }

  edit(id: number) {
    this.router.navigate(['city', 'edit', id]);
  }

  clickFilters(event) {
    this.filters = event;
    this.updateList(1);
  }

}
