import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListCityComponent } from './components/list/list.component';
import { EditCityComponent } from './components/edit/edit.component';


const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'list',
        component: ListCityComponent,
        data: {
          title: 'modules.city.list.title',
          parent: 'city'
        }
      },
      {
        path: 'edit/:id',
        component: EditCityComponent,
        data: {
          title: 'modules.city.edit.title',
          parent: 'city'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CityRoutingModule { }
