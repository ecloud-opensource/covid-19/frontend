import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CityRoutingModule } from './city-routing.module';
import { EditCityComponent } from './components/edit/edit.component';
import { FormCityComponent } from './components/form/form.component';
import { ListCityComponent } from './components/list/list.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { TableModule } from 'src/app/shared/modules/table/table.module';
import { MapModule } from 'src/app/shared/modules/map/map.module';

@NgModule({
  declarations: [
    EditCityComponent,
    FormCityComponent,
    ListCityComponent
  ],
  imports: [
    CommonModule,
    CityRoutingModule,
    SharedModule,
    TableModule,
    MapModule
  ]
})
export class CityModule { }
