import { Component, OnInit, ChangeDetectionStrategy, OnDestroy, Input, ChangeDetectorRef } from '@angular/core';
import { DashboardService } from 'src/app/core/services/dashboard/dashboard.service';
import { fromEvent, Subscription } from 'rxjs';
import { Graph } from 'src/app/models/graph';

@Component({
  selector: 'app-graph-registered-vs-colaborators',
  templateUrl: './graph-registered-vs-colaborators.component.html',
  styleUrls: ['./graph-registered-vs-colaborators.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class GraphRegisteredVsColaboratorsComponent implements OnInit, OnDestroy {
  width: number;
  subscriptionResizeEvent: Subscription;
  results = [];
  colorScheme = {
    domain: ['#007cc9', '#00bc61', '#e04d4d']
  };
  legend = true;
  showLabels = true;
  animations = true;
  xAxis = true;
  yAxis = true;
  timeline = true;

  @Input() zone;

  constructor(
    private dashboardService: DashboardService,
    private cdr: ChangeDetectorRef
    ) { }

  ngOnInit() {
    this.getGraph();
  }

  getGraph(filterData?) {
    this.setGraphicsDimensions();
    const filters = [];
    if (filterData && filterData.idCity) {
      filters['idCity'] = filterData.idCity;
    } else if (filterData && filterData.idState) {
      filters['idState'] = filterData.idState;
    } else if (filterData && filterData.idCountry) {
      filters['idCountry'] = filterData.idCountry;
    }
    this.dashboardService.getGraph(filters).subscribe(element => {
      this.results = new Graph(element.data).items;
      this.cdr.detectChanges();
    });
  }

  setGraphicsDimensions() {
    this.width = window.innerWidth * 0.65;
    const resizeEvent = fromEvent(window, 'resize');
    this.subscriptionResizeEvent = resizeEvent.subscribe(e => {
      this.width = e.target['innerWidth'] * 0.65;
    });
  }

  ngOnDestroy() {
    this.subscriptionResizeEvent.unsubscribe();
  }
}
