import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalAddCaseComponent } from 'src/app/shared/components/modal-add-case/modal-add-case.component';
import { DashboardService } from '../../../../core/services/dashboard/dashboard.service';
import { DashboardMain } from 'src/app/models/dashboard-main';
import { BaseResponse } from 'src/app/models/base-response';
import { Fellows } from '../../../../models/fellows';

@Component({
  selector: 'app-statistics-total-dashboard',
  templateUrl: './statistics-total.component.html',
  styleUrls: ['./statistics-total.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StatisticsTotalDashboardComponent implements OnInit {
  fellows: Fellows;
  misbehavior: number;

  constructor(
    private modalService: NgbModal,
    private dashboardService: DashboardService,
    private cdr: ChangeDetectorRef,
  ) { }

  ngOnInit() {
    this.updateDashboardMain();
  }

  addCase() {
    this.modalService.open(ModalAddCaseComponent);
  }

  updateDashboardMain(filterData?) {
    const filters = [];
    if (filterData && filterData.idCity) {
      filters['idCity'] = filterData.idCity;
    } else if (filterData && filterData.idState) {
      filters['idState'] = filterData.idState;
    } else if (filterData && filterData.idCountry) {
      filters['idCountry'] = filterData.idCountry;
    }
    this.dashboardService.getDashboardMain(filters).subscribe( response => {
      const results = new BaseResponse<DashboardMain>(response, DashboardMain);
      this.fellows = results.data['fellows'];
      this.misbehavior = results.data['misbehavior'];
      this.cdr.detectChanges();
    });
  }
}
