import { Component, OnInit, ChangeDetectionStrategy, Input, ChangeDetectorRef } from '@angular/core';
import { CityService } from 'src/app/core/services/city/city.service';
import { BaseResponse } from 'src/app/models/base-response';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Ranking } from 'src/app/models/ranking';

@Component({
  selector: 'app-statistics-zone-dashboard',
  templateUrl: './statistics-zone.component.html',
  styleUrls: ['./statistics-zone.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StatisticsZoneDashboardComponent implements OnInit {
  @Input() filters;

  cities: Ranking[] = [];
  searchForm: FormGroup;

  constructor(
    private cdr: ChangeDetectorRef,
    private cityService: CityService,
    private fb: FormBuilder
  ) {
    this.searchForm = this.fb.group({
      search: ['', Validators.required]
    });
  }

  ngOnInit() {
    this.getCityRanking();
  }

  getCityRanking() {
    const filters = {
      term: this.searchForm.get('search').value ? this.searchForm.get('search').value : ''
    };
    if (this.filters && this.filters.idCity) {
      filters['idCity'] = this.filters.idCity;
    } else if (this.filters && this.filters.idState) {
      filters['idState'] = this.filters.idState;
    } else if (this.filters && this.filters.idCountry) {
      filters['idCountry'] = this.filters.idCountry;
    }
    this.cityService.ranking(filters).subscribe(response => {
      this.cities = new BaseResponse<Ranking>(response, Ranking).data['rows'];
      this.cdr.detectChanges();
    });
  }

  updateRankingList() {
    this.getCityRanking();
  }

}
