import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MapLatitudeCenter, MapLongitudeCenter, MapZoom, MapZoomCity, MapZoomCountry } from 'src/app/helper/config';
import { MapFilter } from 'src/app/models/map-filter';
import { CountryService } from 'src/app/core/services/country/country.service';
import { CityService } from 'src/app/core/services/city/city.service';
import { Country } from 'src/app/models/country';
import { BaseResponse } from 'src/app/models/base-response';
import { City } from 'src/app/models/city';
import { EndpointsConstants } from 'src/app/helper/endpoints';
import { isNull, isUndefined, generateOptionsByObject, isNullOrEmptyOrUndefined } from 'src/app/helper/utils';
import { GeoService } from 'src/app/core/services/geo/geo.service';
import { getMarker } from 'src/app/helper/map';
import { CatalogUserStatuses } from 'src/app/helper/catalog';
import { StatisticsZoneDashboardComponent } from '../statistics-zone/statistics-zone.component';
import { StatisticsTotalDashboardComponent } from '../statistics-total/statistics-total.component';
import { GraphRegisteredVsColaboratorsComponent } from '../graph-registered-vs-colaborators/graph-registered-vs-colaborators.component';
import { State } from 'src/app/models/state';
import { StateService } from 'src/app/core/services/state/state.service';

@Component({
  selector: 'app-general-dashboard',
  templateUrl: './general.component.html',
  styleUrls: ['./general.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class GeneralDashboardComponent implements OnInit {
  catalogUserStatuses = generateOptionsByObject(CatalogUserStatuses, 'key', 'label');
  filterForm: FormGroup;
  mapLat = MapLatitudeCenter;
  mapLng = MapLongitudeCenter;
  mapZoom = MapZoom;
  showLoader = false;
  filters: object = {
    heatmap: false,
    onMovement: true,
    inHome: true
  };
  filtersMap: MapFilter[] = [
    {
      key: 'heatmap',
      value: false,
      type: 'checkbox',
      name: 'Concentración de afectados'
    },
    {
      key: 'onMovement',
      value: true,
      type: 'checkbox',
      name: 'En movimiento'
    },
    {
      key: 'inHome',
      value: true,
      type: 'checkbox',
      name: 'En domicilio'
    }
  ];
  countries: Country[] = [];
  states: State[] = [];
  cities: City[] = [];
  heatmap: object[] = [];
  markers: object[] = [];
  bounds;
  mapData: object[] = [];
  @ViewChild('statisticsZoneDashboardComponent', { static: false }) statisticsZoneDashboardComponent: StatisticsZoneDashboardComponent;
  @ViewChild('statisticsTotalDashboardComponent', { static: false }) statisticsTotalDashboardComponent: StatisticsTotalDashboardComponent;
  @ViewChild('graphRegisteredVsColaboratorsComponent', { static: false }) graphRegisteredVsColaboratorsComponent: GraphRegisteredVsColaboratorsComponent;

  constructor(
    private cdr: ChangeDetectorRef,
    private fb: FormBuilder,
    private countryService: CountryService,
    private stateService: StateService,
    private cityService: CityService,
    private geoService: GeoService
  ) {
    this.filterForm = this.fb.group({
      idCountry: [null],
      idState: [null],
      idCity: [null],
      status: [null]
    });
  }

  ngOnInit() {
    this.getCountries();
  }

  getCountries(term?) {
    const filters = {
      term: term ? term : ''
    };
    this.countryService.get<Country>(1, EndpointsConstants.requestLimit, filters).subscribe(response => {
      this.countries = new BaseResponse<Country>(response, Country).data['rows'];
      this.cdr.detectChanges();
    });
  }

  onCountryChange(country) {
    if (!isUndefined(country)) {
      this.getStates(country.id);
      this.getCities(country.id);
    } else {
      this.cities = [];
      this.states = [];
    }
    this.filterForm.get('idCity').setValue(null);
    this.filterForm.get('idState').setValue(null);
    this.cdr.detectChanges();
  }

  onStateChange(state) {
    if (!isUndefined(state)) {
      this.getCities(this.filterForm.get('idCountry').value, state.id);
    } else {
      this.getCities(this.filterForm.get('idCountry').value);
    }
    this.filterForm.get('idCity').setValue(null);
    this.cdr.detectChanges();
  }

  getStates(idCountry, term?) {
    const filters = {
      idCountry,
      term: term ? term : ''
    };
    this.stateService.get<State>(1, EndpointsConstants.requestLimit, filters).subscribe(response => {
      this.states = new BaseResponse<State>(response, State).data['rows'];
      this.cdr.detectChanges();
    });
  }

  getCities(idCountry, idState?, term?) {
    const filters = {
      idCountry,
      term: term ? term : ''
    };
    if (!isNullOrEmptyOrUndefined(idState)) {
      filters['idState'] = idState;
    }
    this.cityService.get<City>(1, EndpointsConstants.requestLimit, filters).subscribe(response => {
      this.cities = new BaseResponse<City>(response, City).data['rows'];
      this.cdr.detectChanges();
    });
  }

  applyFilters() {
    const formModel = this.filterForm.value;
    this.statisticsTotalDashboardComponent.updateDashboardMain(formModel);
    this.graphRegisteredVsColaboratorsComponent.getGraph(formModel);
    this.statisticsZoneDashboardComponent.getCityRanking();
    this.changeMapBounds();
  }

  changeMapFilters(event) {
    this.filters = event;
    this.getMapData();
  }

  changeMapBounds() {
    const idCountry = this.filterForm.value.idCountry;
    const idCity = this.filterForm.value.idCity;
    let location;
    let zoom;
    // Find city or country
    if (!isNull(idCity)) {
      location = this.cities.find( item => item.id === idCity);
      zoom = MapZoomCity;
    } else if (!isNull(idCountry)) {
      location = this.countries.find( item => item.id === idCountry);
      zoom = MapZoomCountry;
    }
    // Replace map location if it is necessary
    this.mapZoom = !isUndefined(zoom) ? zoom : MapZoom;
    this.mapLat = !isUndefined(location) ? location.center.coordinates[0] : MapLatitudeCenter;
    this.mapLng = !isUndefined(location) ? location.center.coordinates[1] : MapLongitudeCenter;
  }

  onBoundsChange(bounds) {
    this.bounds = bounds;
    this.getGeoService();
  }

  getGeoService() {
    this.changeLoaderVisibility(true);
    const params = {
      ...this.bounds
    };
    const status = this.filterForm.get('status').value;
    if (!isNull(status)) {
      params['status'] = status;
    }
    this.geoService.getAdminNearBy(params).subscribe( response => {
      this.mapData = response.data['users'];
      this.getMapData();
      this.changeLoaderVisibility(false);
    });
  }

  getMapData() {
    this.getMarkers();
    this.getHeatmap();
    this.cdr.detectChanges();
  }

  getMarkers() {
    this.markers = [];
    // Filter on movement
    const filterOnMovement = this.mapData.filter( item => item['onMovement'] === this.filters['onMovement']);
    // Filter in home
    const filterInHome = this.mapData.filter( item => item['outOfSafehouse'] !== this.filters['inHome']);
    // Concat filters
    const data = filterOnMovement.concat(filterInHome);
    data.forEach( user => {
      // Avoid repeated markers
      const isMarkerAddedPreviously = this.markers.find( item => item['latitude'] === user['latestPosition'].position.coordinates[0] && item['longitude'] === user['latestPosition'].position.coordinates[1]);
      if (!isNull(user['status']) && !isMarkerAddedPreviously) {
        this.markers.push(getMarker(user, user['latestPosition'].position));
      }
    });
  }

  getHeatmap() {
    if (!this.filters['heatmap']) {
      this.heatmap = [];
    } else {
      const data = [];
      this.mapData.forEach( user => {
        data.push(new google.maps.LatLng(user['latestPosition'].position.coordinates[0], user['latestPosition'].position.coordinates[1]));
      });
      this.heatmap = data;
    }
  }

  changeLoaderVisibility(isVisible) {
    this.showLoader = isVisible;
    this.cdr.detectChanges();
  }
}
