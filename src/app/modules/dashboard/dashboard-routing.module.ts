import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GeneralDashboardComponent } from './components/general/general.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: GeneralDashboardComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
