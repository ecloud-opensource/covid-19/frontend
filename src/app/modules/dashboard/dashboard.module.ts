import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { MapModule } from 'src/app/shared/modules/map/map.module';
import { StatisticsTotalDashboardComponent } from './components/statistics-total/statistics-total.component';
import { GraphRegisteredVsColaboratorsComponent } from './components/graph-registered-vs-colaborators/graph-registered-vs-colaborators.component';
import { GeneralDashboardComponent } from './components/general/general.component';
import { StatisticsZoneDashboardComponent } from './components/statistics-zone/statistics-zone.component';
import { ModalAddCaseComponent } from 'src/app/shared/components/modal-add-case/modal-add-case.component';

@NgModule({
  declarations: [
    GeneralDashboardComponent,
    StatisticsZoneDashboardComponent,
    GraphRegisteredVsColaboratorsComponent,
    StatisticsTotalDashboardComponent
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    SharedModule,
    NgxChartsModule,
    MapModule
  ],
  entryComponents: [
    ModalAddCaseComponent
  ]
})
export class DashboardModule { }
