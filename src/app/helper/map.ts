import { getClassByStatus } from './utils';

export function getMarker(data, position) {
    return {
        status: getClassByStatus(data.status),
        latitude: position.coordinates[0],
        longitude: position.coordinates[1]
    };
}

export function getRoute(position) {
    return {
        latitude: position.coordinates[0],
        longitude: position.coordinates[1]
    };
}
