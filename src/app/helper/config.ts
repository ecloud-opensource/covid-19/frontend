export const DefaultAvatarUrl = './assets/img/avatar.png';

export const MapLatitudeCenter = 0;

export const MapLongitudeCenter = 0;

export const MapMinZoom = 2;

export const MapZoom = 2;

export const MapZoomCountry = 3;

export const MapZoomState = 10;

export const MapZoomCity = 10;

export const MapZoomNeighborhood = 11;

export const DefaultRefreshMapTime = 30;

export const SecondaryColor = '#76777A';
