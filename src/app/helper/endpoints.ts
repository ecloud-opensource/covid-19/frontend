export const EndpointsConstants = {
  requestLimit: 20,
  shortRequestLimit: 12,
  admin: {
    base: '/admins',
    me: '/me',
    login: '/login',
    forgotPassword: '/forgot-password',
    resetPassword: '/reset-password',
    register: '/register'
  },
  country: {
    base: '/country',
    log: '/log'
  },
  city: {
    base: '/city',
    log: '/log',
    ranking: '/admin-ranking'
  },
  state: {
    base: '/state',
    log: '/log'
  },
  neighborhood: {
    base: '/neighborhood',
    log: '/log',
    ranking: '/admin-ranking'
  },
  link: {
    base: '/links'
  },
  misbehavior: {
    base: '/misbehavior'
  },
  setting: {
    base: '/setting'
  },
  upload: {
    base: '/upload',
    image: '/image'
  },
  user: {
    base: '/user',
    questionnaire: '/admin-questionnaire',
    position: '/position'
  },
  cityFeed: {
    base: '/city-feed'
  },
  geo: {
    base: '/geo',
    adminNearBy: '/admin-nearby'
  },
  dashboard: {
    base: '/dashboard',
    main: '/main',
    graph: '/graph'
  }
};
