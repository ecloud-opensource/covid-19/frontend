export enum CatalogUploadTypeClass {
    'avatar' = 'avatar'
}

export enum CatalogLogFields {
    areInfected = 'areInfected'
}

export enum CatalogResponsiveBreakpoints {
    desktop = 1200,
    laptop = 991,
    tablet = 768,
    smart = 576
}

export enum CatalogApiScopes {
    city = 'city',
    country = 'country',
    user = 'user',
    neighborhood = 'neighborhood',
    latestPosition = 'latestPosition',
    positionsWithMatch = 'positionsWithMatch'
}

export const CatalogUserStatuses = {
    quarantined: {
        label: 'En cumplimiento medida instaurada',
        key: 'QUARANTINED'
    },
    onRisk: {
        label: 'Grupo de riesgo',
        key: 'ON_RISK'
    },
    exposed: {
        label: 'Contacto',
        key: 'EXPOSED'
    },
    withSymptoms: {
        label: 'Con síntomas',
        key: 'WITH_SYMPTOMS'
    },
    healthy: {
        label: 'Sin síntomas',
        key: 'HEALTHY'
    }
};

export const CatalogQuestionnaireTopics = [
    {
        key: 'A',
        label: 'Enfermedades Previas',
        isTitle: true
    },
    {
        key: 'A.1',
        label: 'Diabetes',
        isTitle: false
    },
    {
        key: 'A.2',
        label: 'Inmunosupresión congénita o adquirida',
        isTitle: false
    },
    {
        key: 'A.3',
        label: 'Obesidad',
        isTitle: false
    },
    {
        key: 'A.4',
        label: 'Puerperio',
        isTitle: false
    },
    {
        key: 'A.5',
        label: 'Enfermedad neurológica',
        isTitle: false
    },
    {
        key: 'A.6',
        label: 'Enfermedad hepática',
        isTitle: false
    },
    {
        key: 'A.7',
        label: 'Enfermedad Renal Crónica',
        isTitle: false
    },
    {
        key: 'A.8',
        label: 'Hipertensión arterial',
        isTitle: false
    },
    {
        key: 'A.9',
        label: 'Insuficiencia cardíaca',
        isTitle: false
    },
    {
        key: 'A.10',
        label: 'Enfermedad oncológica',
        isTitle: false
    },
    {
        key: 'B',
        label: 'Antecedentes epidemiológicos',
        isTitle: true
    },
    {
        key: 'B.1',
        label: 'Trabajador de atención de la salud',
        isTitle: false
    },
    {
        key: 'B.2',
        label: 'Trabajador de laboratorio',
        isTitle: false
    },
    {
        key: 'B.3',
        label: 'Trabaja con animales',
        isTitle: false
    },
    {
        key: 'C',
        label: 'Signos y síntomas',
        isTitle: true
    },
    {
        key: 'C.1',
        label: 'Fiebre (≥38ºC)',
        isTitle: false
    },
    {
        key: 'C.2',
        label: 'Tos',
        isTitle: false
    },
    {
        key: 'C.3',
        label: 'Dificultad respiratoria',
        isTitle: false
    },
    {
        key: 'C.4',
        label: 'Odinofagia (dolor de garganta)',
        isTitle: false
    },
    {
        key: 'C.5',
        label: 'Dolor abdominal',
        isTitle: false
    },
    {
        key: 'C.6',
        label: 'Cefalea',
        isTitle: false
    },
    {
        key: 'C.7',
        label: 'Malestar general',
        isTitle: false
    },
    {
        key: 'C.A',
        label: 'Viajes y otras exposiciones riesgo',
        isTitle: true
    },
    {
        key: 'C.A.1',
        label: '¿Ha viajado o residido en una zona de riesgo conocida fuera del país en los últimos 14 días previos al inicio de síntomas?',
        isTitle: false
    },
    {
        key: 'C.A.1.A',
        label: '¿Donde?',
        isTitle: false
    },
    {
        key: 'C.A.1.B',
        label: '¿Desde?',
        isTitle: false
    },
    {
        key: 'C.A.1.C',
        label: '¿Hasta?',
        isTitle: false
    },
    {
        key: 'C.A.1.D',
        label: '¿Viajo en avión?',
        isTitle: false
    },
    {
        key: 'C.A.1.E',
        label: '¿Viajo en Barco?',
        isTitle: false
    },
    {
        key: 'C.A.1.F',
        label: '¿Viajo en Omnibus?',
        isTitle: false
    },
    {
        key: 'C.A.1.G',
        label: '¿Fecha de ingreso al pais?',
        isTitle: false
    },
    {
        key: 'C.A.1.H',
        label: '¿Compañia?',
        isTitle: false
    },
    {
        key: 'C.A.2',
        label: '¿Ha viajado o residido en una zona dentro del país (distinto del domicilio) en los últimos 14 días previos al inicio de síntomas?',
        isTitle: false
    },
    {
        key: 'C.A.2.A',
        label: '¿Donde?',
        isTitle: false
    },
    {
        key: 'C.A.2.B',
        label: '¿Desde?',
        isTitle: false
    },
    {
        key: 'C.A.2.C',
        label: '¿Hasta?',
        isTitle: false
    },
    {
        key: 'C.A.3',
        label: '¿Ha concurrido a un centro de salud que ha asistido casos confirmados por COVID-19 dentro de los 14 días previos al inicio de los síntomas?',
        isTitle: false
    },
    {
        key: 'C.A.3.A',
        label: 'Nombre del centro',
        isTitle: false
    },
    {
        key: 'C.A.4',
        label: '¿Estuvo en contacto con animales dentro de los 14 días previos al inicio de los síntomas?',
        isTitle: false
    },
    {
        key: 'C.A.4.A',
        label: 'Cerdos',
        isTitle: false
    },
    {
        key: 'C.A.4.B',
        label: 'Aves',
        isTitle: false
    },
    {
        key: 'C.A.4.C',
        label: 'Camélidos',
        isTitle: false
    },
    {
        key: 'C.A.4.D',
        label: 'Mercado de animales vivos',
        isTitle: false
    },
    {
        key: 'C.A.5',
        label: '¿Tuvo contacto cercano con personas con infección respiratoria aguda dentro de los 14 días previos al inicio de síntomas?',
        isTitle: false
    },
    {
        key: 'C.A.5.A',
        label: 'En entorno asistencia',
        isTitle: false
    },
    {
        key: 'C.A.5.B',
        label: 'En entorno familiar',
        isTitle: false
    },
    {
        key: 'C.A.5.C',
        label: 'En entorno laboral',
        isTitle: false
    },
    {
        key: 'C.A.6',
        label: '¿Tuvo contacto estrecho con casos probables o confirmados dentro de los 14 días previos al inicio de síntomas?',
        isTitle: false
    },
    {
        key: 'C.A.6.A',
        label: 'Apellido y nombre del caso',
        isTitle: false
    },
    {
        key: 'C.A.6.B',
        label: 'DNI o DE',
        isTitle: false
    },
    {
        key: 'C.A.6.C',
        label: 'País y área en la que tuvo la exposición',
        isTitle: false
    },
    {
        key: 'D',
        label: 'Datos de personas con las que estuvo en contacto estrecho durante el período sintomático',
        isTitle: true
    },
    {
        key: 'D.1',
        label: 'Apellido y nombre',
        isTitle: false
    },
    {
        key: 'D.2',
        label: 'DNI',
        isTitle: false
    },
    {
        key: 'D.3',
        label: 'Teléfono',
        isTitle: false
    },
    {
        key: 'D.4',
        label: 'Domicilio',
        isTitle: false
    },
    {
        key: 'D.5',
        label: 'Fecha último contacto',
        isTitle: false
    },
    {
        key: 'D.6',
        label: 'Tipo',
        isTitle: false
    }
];
