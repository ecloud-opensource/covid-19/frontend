import { FormGroup } from '@angular/forms';

export function passwordMatchValidator(form: FormGroup) {
    return form.controls.password.value === form.controls.repeatPassword.value ? null : { mismatch: true };
}
