export class MapFilter {
    name: string;
    value: boolean | string | number;
    key: string;
    type: 'checkbox' | 'select';
    options?: object[];
}
