import { BaseEntity } from './base-entity';
import { Point } from './point';
import { Polygon } from './polygon';
import { Country } from './country';
​
export class State extends BaseEntity<State> {
  id: number;
  name: string;
  code: string;
  center?: Point;
  area?: Polygon;
  fellows: number;
  areInfected: number;
  idCountry: number;
  country?: Country;
​
  constructor(state?) {
    super(state);
  }
​
  parse(e: any): State {
    const state = new State(e);
    return state;
  }
​
  populate(state) {
    this.id = state.id;
    this.name = state.name;
    this.code = state.code;
    if (state.center) { this.center = new Point(state.center); }
    if (state.area) { this.area = new Polygon(state.area); }
    this.fellows = state.fellows;
    this.areInfected = state.areInfected;
    this.idCountry = state.idCountry;
    if (state.country) { this.country = new Country(state.country); }
  }
}
