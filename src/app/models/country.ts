import { BaseEntity } from './base-entity';
import { Point } from './point';
import { Polygon } from './polygon';
​
export class Country extends BaseEntity<Country> {
  id: number;
  name: string;
  code: string;
  center?: Point;
  area?: Polygon;
  areInfected: number;
  fellows: number;
​
  constructor(country?) {
    super(country);
  }
​
  parse(e: any): Country {
    const country = new Country(e);
    return country;
  }
​
  populate(country) {
    this.id = country.id;
    this.name = country.name;
    this.code = country.code;
    if (country.center) { this.center = new Point(country.center); }
    if (country.area) { this.area = new Polygon(country.area); }
    this.areInfected = country.areInfected;
    this.fellows = country.fellows;
  }
}
