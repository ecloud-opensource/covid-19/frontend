export class Point {
  type = 'Point';
  coordinates: any[];

  constructor(point) {
    if (point.coordinates) { this.coordinates = point.coordinates; }
  }
}
