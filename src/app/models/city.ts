import { BaseEntity } from './base-entity';
import { Point } from './point';
import { Polygon } from './polygon';
import { Country } from './country';
import { State } from './state';
​
export class City extends BaseEntity<City> {
  id: number;
  name: string;
  code: string;
  center?: Point;
  area?: Polygon;
  fellows: number;
  areInfected: number;
  idCountry: number;
  country?: Country;
  idState: number;
  state?: State;
​
  constructor(city?) {
    super(city);
  }
​
  parse(e: any): City {
    const city = new City(e);
    return city;
  }
​
  populate(city) {
    this.id = city.id;
    this.name = city.name;
    this.code = city.code;
    if (city.center) { this.center = new Point(city.center); }
    if (city.area) { this.area = new Polygon(city.area); }
    this.fellows = city.fellows;
    this.areInfected = city.areInfected;
    this.idCountry = city.idCountry;
    if (city.country) { this.country = new Country(city.country); }
    this.idState = city.idState;
    if (city.state) { this.state = new State(city.state); }
  }
}
