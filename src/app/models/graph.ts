export class Graph {
    items: GraphItem[] = [];

    constructor(data) {
        const infected = new GraphItem('Infectados', data.infected);
        const users = new GraphItem('Colaboradores', data.users);
        this.items.push(infected);
        this.items.push(users);
    }
}

export class GraphItem {
    name: string;
    series: Serie[];

    constructor(name, data) {
        this.series = [];
        this.name = name;

        data.forEach(element => {
            this.series.push(new Serie(element));
        });
    }
}

export class Serie {
    name: string;
    value: number;

    constructor(data) {
        this.name = data.date;
        this.value = data.count;
    }
}
