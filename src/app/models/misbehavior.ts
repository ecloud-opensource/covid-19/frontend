import { BaseEntity } from './base-entity';
import { City } from './city';
import { Country } from './country';
import { Colaborator } from './colaborator';
import { Neighborhood } from './neighborhood';
​
export class Misbehavior extends BaseEntity<Misbehavior> {
    id: number;
    from: Date;
    to: Date;
    user: Colaborator;
    idCountry: number;
    idCity: number;
    idNeighborhood: number;
​
  constructor(misbehavior?) {
    super(misbehavior);
  }
​
  parse(e: any): Misbehavior {
    const misbehavior = new Misbehavior(e);
    return misbehavior;
  }
​
  populate(misbehavior) {
      this.id = misbehavior.id;
      this.from = new Date(misbehavior.from);
      this.to = new Date(misbehavior.to);
      this.user = new Colaborator(misbehavior.user);
      this.idCountry = misbehavior.idCountry;
      this.idCity = misbehavior.idCity;
      this.idNeighborhood = misbehavior.idNeighborhood;
  }
}
