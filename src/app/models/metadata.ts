export class Metadata {
    totalResults: number;
    currentPage: number;
    pageSize: number;

    constructor(metadata) {
        this.totalResults = metadata.totalResults;
        this.currentPage = metadata.currentPage;
        this.pageSize = metadata.pageSize;
    }
}
