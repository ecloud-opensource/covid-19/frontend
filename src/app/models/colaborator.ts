import { BaseEntity } from './base-entity';
import { isNull } from 'util';
import { generateOptionsByObject } from '../helper/utils';
import { CatalogUserStatuses } from '../helper/catalog';
import * as moment from 'moment';
import { LatestPosition } from './latest-position';
import { City } from './city';
import { Neighborhood } from './neighborhood';
import { Point } from './point';
​
export class Colaborator extends BaseEntity<Colaborator> {
  id: number;
  firstName: string;
  lastName: string;
  fullName?: string;
  identification: string;
  phone: string;
  email: string;
  address: string;
  number: number;
  status: string;
  statusLabel: string;
  birthdate: Date;
  age?: number;
  idCity: number;
  city?: City;
  idNeighborhood: number;
  neighborhood?: Neighborhood;
  safehouse?: Point;
  isQuarantined: boolean;
  questionnaireAnswered: boolean;
  idLatestPosition: number;
  latestPosition: LatestPosition;
  createdAt: Date;
​
  constructor(colaborator?) {
    super(colaborator);
  }
​
  parse(e: any): Colaborator {
    const colaborator = new Colaborator(e);
    return colaborator;
  }
​
  populate(colaborator) {
    this.id = colaborator.id;
    this.firstName = colaborator.firstName;
    this.lastName = colaborator.lastName;
    if (!isNull(colaborator.firstName) && !isNull(colaborator.lastName)) {
      this.fullName =  `${colaborator.firstName} ${colaborator.lastName}`;
    }
    this.identification = colaborator.identification;
    this.phone = colaborator.phone;
    this.email = colaborator.email;
    this.address = colaborator.address;
    this.number = colaborator.number;
    this.status = colaborator.status;
    const statuses = generateOptionsByObject(CatalogUserStatuses, 'label', 'key');
    this.statusLabel = colaborator.status ? statuses.find( item => item.label === colaborator.status).value : '';
    this.birthdate = colaborator.birthdate;
    if (colaborator.birthdate) { this.age = moment(new Date(), 'DD-MM-YYYY').diff(colaborator.birthdate, 'years'); }
    this.idCity = colaborator.idCity;
    if (colaborator.city) { this.city = new City(colaborator.city); }
    this.idNeighborhood = colaborator.idNeighborhood;
    if (colaborator.neighborhood) { this.neighborhood = new Neighborhood(colaborator.neighborhood); }
    if (colaborator.safehouse) { this.safehouse = new Point(colaborator.safehouse); }
    this.isQuarantined = colaborator.isQuarantined;
    this.questionnaireAnswered = colaborator.questionnaireAnswered;
    this.idLatestPosition = colaborator.idLatestPosition;
    if (colaborator.latestPosition) { this.latestPosition = new LatestPosition(colaborator.latestPosition); }
    this.createdAt = colaborator.createdAt;
  }
}
