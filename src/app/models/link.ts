import { BaseEntity } from './base-entity';
import { City } from './city';
​
export class Link extends BaseEntity<Link> {
  id: number;
  url: string;
  title: string;
  image: string;
  description: string;
  order: number;
  city?: City;
​  idCity: number;

  constructor(link?) {
    super(link);
  }
​
  parse(e: any): Link {
    const link = new Link(e);
    return link;
  }
​
  populate(link) {
    this.id = link.id;
    this.url = link.url;
    this.title = link.title;
    this.description = link.description;
    this.order = link.order;
    this.image = link.image;
    this.idCity = link.idCity;
    if (link.city) { this.city = new City(link.city); }
  }
}
