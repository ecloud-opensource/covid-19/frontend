export class Polygon {
  type = 'Polygon';
  coordinates: number[][][];

  constructor(spotType) {
    if (spotType.coordinates) { this.coordinates = spotType.coordinates; }
  }
}
