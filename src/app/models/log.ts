import { BaseEntity } from './base-entity';
import { CatalogLogFields } from '../helper/catalog';
​
export class Log extends BaseEntity<Log> {
  id: number;
  oldValue: string;
  newValue: string;
  field: string;
  fieldTranslate: string;
  createdAt: Date;
​
  constructor(log?) {
    super(log);
  }
​
  parse(e: any): Log {
    const log = new Log(e);
    return log;
  }
​
  populate(log) {
    this.id = log.id;
    this.oldValue = log.oldValue;
    this.newValue = log.newValue;
    this.field = log.field;
    this.createdAt = new Date(log.createdAt);
  }
}
