import { BaseEntity } from './base-entity';
​
export class Fellows extends BaseEntity<Fellows> {
    QUARANTINED: number;
    ON_RISK: number;
    EXPOSED: number;
    WITH_SYMPTOMS: number;
    HEALTHY: number;

  constructor(fellows?) {
    super(fellows);
  }
​
  parse(e: any): Fellows {
    const fellows = new Fellows(e);
    return fellows;
  }
​
  populate(fellows) {
    this.QUARANTINED = fellows.QUARANTINED;
    this.ON_RISK = fellows.ON_RISK;
    this.EXPOSED = fellows.EXPOSED;
    this.WITH_SYMPTOMS = fellows.WITH_SYMPTOMS;
    this.HEALTHY = fellows.HEALTHY;
  }
}
