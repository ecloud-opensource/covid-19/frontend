export class Polyline {
    type = 'Polyline';
    coordinates: number[][][];

    constructor(spotType) {
      if (spotType.coordinates) { this.coordinates = spotType.coordinates; }
    }
  }
