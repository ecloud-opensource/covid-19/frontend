import { BaseEntity } from './base-entity';

export class UploadedFile extends BaseEntity<UploadedFile>  {
    path: string;

    constructor(uploadedFile?) {
        super(uploadedFile);
    }

    parse(e: any): UploadedFile {
        const uploadedFile = new UploadedFile(e);
        return uploadedFile;
    }

    populate(uploadedFile) {
        this.path = uploadedFile.path;
    }
}
