import { BaseEntity } from './base-entity';
import { Point } from './point';
import { Polygon } from './polygon';
import { City } from './city';
​
export class Neighborhood extends BaseEntity<Neighborhood> {
  id: number;
  name: string;
  code: string;
  center?: Point;
  area?: Polygon;
  fellows: number;
  areInfected: number;
  idCity: number;
  city?: City;
​
  constructor(neighborhood?) {
    super(neighborhood);
  }
​
  parse(e: any): Neighborhood {
    const neighborhood = new Neighborhood(e);
    return neighborhood;
  }
​
  populate(neighborhood) {
    this.id = neighborhood.id;
    this.name = neighborhood.name;
    this.code = neighborhood.code;
    if (neighborhood.center) { this.center = new Point(neighborhood.center); }
    if (neighborhood.area) { this.area = new Polygon(neighborhood.area); }
    this.fellows = neighborhood.fellows;
    this.areInfected = neighborhood.areInfected;
    this.idCity = neighborhood.idCity;
    if (neighborhood.city) { this.city = new City(neighborhood.city); }
  }
}
