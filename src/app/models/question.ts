import { BaseEntity } from './base-entity';
​
export class Question extends BaseEntity<Question> {
    id: number;
    createdAt: Date;
    updatedAt: Date;
    isDeleted: boolean;
    answer: string;
    question: string;
    idUser: number;
​
  constructor(question?) {
    super(question);
  }
​
  parse(e: any): Question {
    const question = new Question(e);
    return question;
  }
​
  populate(question) {
      this.id = question.id;
      this.createdAt = new Date(question.createdAt);
      this.updatedAt = new Date(question.updatedAt);
      this.isDeleted = question.isDeleted;
      this.answer = question.answer;
      this.question = question.question;
      this.idUser = question.idUser;
  }
}
