import { BaseEntity } from './base-entity';
import { Colaborator } from './colaborator';
​
export class Match extends BaseEntity<Match> {
  id: number;
  status: string;
  outOfSafehouse: boolean;
  onMovement: boolean;
  isQuarantined: boolean;
  distance: boolean;
  user: Colaborator;
​
  constructor(match?) {
    super(match);
  }
​
  parse(e: any): Match {
    const match = new Match(e);
    return match;
  }
​
  populate(match) {
    this.id = match.id;
    this.status = match.status;
    this.outOfSafehouse = match.outOfSafehouse;
    this.onMovement = match.onMovement;
    this.isQuarantined = match.isQuarantined;
    this.distance = match.distance;
    this.user = new Colaborator(match.user);
  }
}
