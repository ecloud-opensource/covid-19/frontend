import { BaseEntity } from './base-entity';
​
export class Setting extends BaseEntity<Setting> {
  id: number;
  name: string;
  code: string;
  description: string;
  value: string;
​
  constructor(setting?) {
    super(setting);
  }
​
  parse(e: any): Setting {
    const setting = new Setting(e);
    return setting;
  }
​
  populate(setting) {
    this.id = setting.id;
    this.name = setting.name;
    this.code = setting.code;
    this.description = setting.description;
    this.value = setting.value;
  }
}
