import { BaseEntity } from './base-entity';
import { Fellows } from './fellows';
​
export class DashboardMain extends BaseEntity<DashboardMain> {
  fellows: Fellows;
  misbehavior: number;

​
  constructor(dashboardMain?) {
    super(dashboardMain);
  }
​
  parse(e: any): DashboardMain {
    const dashboardMain = new DashboardMain(e);
    return dashboardMain;
  }
​
  populate(dashboardMain) {
    if (dashboardMain.fellows) { this.fellows = new Fellows(dashboardMain.fellows); }
    this.misbehavior = dashboardMain.misbehavior;
  }
}
