import { BaseEntity } from './base-entity';
import { Point } from './point';
import { Match } from './match';
​
export class Position extends BaseEntity<Position> {
  id: number;
  position: Point;
  nearQuarantine: boolean;
  outOfSafehouse: boolean;
  onMovement: boolean;
  match?: Match[];
​
  constructor(position?) {
    super(position);
  }
​
  parse(e: any): Position {
    const position = new Position(e);
    return position;
  }
​
  populate(position) {
    this.id = position.id;
    this.position = new Point(position.position);
    this.nearQuarantine = position.nearQuarantine;
    this.outOfSafehouse = position.outOfSafehouse;
    this.onMovement = position.onMovement;
    if (position.match) { this.match = this.setMatch(position.match); }
  }

  setMatch(matchs) {
    const temp = [];
    matchs.forEach(match => {
      temp.push(new Match(match));
    });
    return temp;
  }
}
