import { BaseEntity } from './base-entity';
import { City } from 'src/app/models/city';

export class CityFeed extends BaseEntity<CityFeed> {
  id?: number;
  url: string;
  title: string;
  image: string;
  description: string;
  order: number;
  idCity: number;
  city?: City;

  constructor(cityFeed?) {
    super(cityFeed);
  }

  parse(e: any): CityFeed {
    const cityFeed = new CityFeed(e);
    return cityFeed;
  }

  populate(cityFeed) {
    this.id = cityFeed.id;
    this.title = cityFeed.title;
    this.url = cityFeed.url;
    this.image = cityFeed.image;
    this.description = cityFeed.description;
    this.order = cityFeed.order;
    this.idCity = cityFeed.idCity;
    if (cityFeed.city) { this.city = new City(cityFeed.city); }
  }
}
