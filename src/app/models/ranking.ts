import { BaseEntity } from './base-entity';
​
export class Ranking extends BaseEntity<Ranking> {
  id: number;
  name: string;
  fellows: number;
  areInfected: number;
  areQuarantined: number;
  areOnRisk: number;
  areExposed: number;
  areWithSymptoms: number;
  areHealthy: number;
​
  constructor(ranking?) {
    super(ranking);
  }
​
  parse(e: any): Ranking {
    const ranking = new Ranking(e);
    return ranking;
  }
​
  populate(ranking) {
    this.id = ranking.id;
    this.name = ranking.name;
    this.fellows = ranking.fellows;
    this.areInfected = ranking.areInfected;
    this.areQuarantined = ranking.areQuarantined;
    this.areOnRisk = ranking.areOnRisk;
    this.areExposed = ranking.areExposed;
    this.areWithSymptoms = ranking.areWithSymptoms;
    this.areHealthy = ranking.areHealthy;
  }
}
