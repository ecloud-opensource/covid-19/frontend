import { Injectable } from '@angular/core';
import { Ng2IzitoastService } from 'ng2-izitoast';
import { TranslateService } from '@ngx-translate/core';
import { isEmpty } from 'src/app/helper/utils';

@Injectable({
  providedIn: 'root'
})
export class ToastService {

  constructor(
    private iziToast: Ng2IzitoastService,
    private translateService: TranslateService
  ) { }

  success(msg = '', title = '') {
    this.iziToast.success({
      title: !isEmpty(title) ? this.translateService.instant(title) : '',
      message: !isEmpty(msg) ? this.translateService.instant(msg) : '',
      position: 'topRight',
      transitionIn: 'bounceInDown',
      transitionOut: 'fadeOutRight'
    });
  }

  info(msg = '', title = '') {
    this.iziToast.info({
      title: !isEmpty(title) ? this.translateService.instant(title) : '',
      message: !isEmpty(msg) ? this.translateService.instant(msg) : '',
      position: 'topRight',
      transitionIn: 'bounceInDown',
      transitionOut: 'fadeOutRight'
    });
  }

  danger(msg = '', title = 'ERROR') {
    this.iziToast.error({
      title: !isEmpty(title) ? this.translateService.instant(title) : '',
      message: !isEmpty(msg) ? this.translateService.instant(msg) : '',
      position: 'topRight',
      transitionIn: 'bounceInDown',
      transitionOut: 'fadeOutRight'
    });
  }
}
