import { Injectable } from '@angular/core';
import { HttpService } from '../http/http.service';
import { EndpointsConstants } from 'src/app/helper/endpoints';
import { ApiService } from '../api/api.service';
import { Observable } from 'rxjs';
import { BaseResponse } from 'src/app/models/base-response';
import { Admin } from 'src/app/models/admin';
import { generateParamsForRoutes } from 'src/app/helper/utils';

@Injectable({
  providedIn: 'root'
})
export class AdminService extends ApiService {

  constructor(httpService: HttpService) {
    super(httpService);
    this.baseUrl = EndpointsConstants.admin.base;
  }

  putMe(admin): Observable<BaseResponse<Admin>> {
    const url = this.baseUrl + EndpointsConstants.admin.me;
    return this.httpService.put<Admin>(url, admin, true);
  }

  getMe(filters: any = []): Observable<BaseResponse<Admin>> {
    const url = this.baseUrl + EndpointsConstants.admin.me;
    const queryParams = generateParamsForRoutes(filters);
    return this.httpService.get<Admin>(url, queryParams, true);
  }

}
