import { Injectable } from '@angular/core';
import { HttpService } from '../http/http.service';
import { EndpointsConstants } from 'src/app/helper/endpoints';
import { ApiService } from '../api/api.service';
import { Observable } from 'rxjs/internal/Observable';
import { Question } from 'src/app/models/question';
import { BaseResponse } from 'src/app/models/base-response';
import { generateParamsForRoutes } from 'src/app/helper/utils';
import { Position } from 'src/app/models/position';

@Injectable({
  providedIn: 'root'
})
export class UserService extends ApiService {

  constructor(httpService: HttpService) {
    super(httpService);
    this.baseUrl = EndpointsConstants.user.base;
  }

  getQuestionnaire(idUser: number): Observable<BaseResponse<Question>> {
    const url = this.baseUrl + EndpointsConstants.user.questionnaire + '/' + idUser;
    return this.httpService.get(url, true);
  }

  position(filters: any = []) {
    const url = this.baseUrl + EndpointsConstants.user.position;
    const params = generateParamsForRoutes(filters);
    return this.httpService.get<Position>(url, params, true);
  }

}
