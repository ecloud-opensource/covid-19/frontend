import { Injectable } from '@angular/core';
import { HttpService } from '../http/http.service';
import { EndpointsConstants } from 'src/app/helper/endpoints';
import { ApiService } from '../api/api.service';

@Injectable({
  providedIn: 'root'
})
export class CityFeedService extends ApiService {

  constructor(httpService: HttpService) {
    super(httpService);
    this.baseUrl = EndpointsConstants.cityFeed.base;
  }

}
