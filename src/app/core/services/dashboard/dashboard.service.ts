import { Injectable } from '@angular/core';
import { ApiService } from '../api/api.service';
import { EndpointsConstants } from 'src/app/helper/endpoints';
import { BaseEntity } from 'src/app/models/base-entity';
import { Observable } from 'rxjs';
import { BaseResponse } from 'src/app/models/base-response';
import { HttpService } from '../http/http.service';
import { DashboardMain } from '../../../models/dashboard-main';
import { Graph } from '../../../models/graph';

@Injectable({
  providedIn: 'root'
})
export class DashboardService extends ApiService {

    data = [
        {
          name: 'Colaboradores',
          series: [
            {
              name: '2020-03-01',
              value: 10
            },
            {
              name: '2020/03/02',
              value: 20
            },
            {
              name: '2020/03/03',
              value: 40
            },
            {
              name: '2020/03/04',
              value: 80
            },
            {
              name: '2020/03/05',
              value: 160
            },
            {
              name: '2020/03/06',
              value: 320
            },
            {
              name: '2020/03/07',
              value: 640
            },
            {
              name: '2020/03/08',
              value: 1280
            }
          ]
        },
        {
          name: 'Registrados',
          series: [
            {
              name: '2020-03-01',
              value: 100
            },
            {
              name: '2020/03/02',
              value: 150
            },
            {
              name: '2020/03/03',
              value: 200
            },
            {
              name: '2020/03/04',
              value: 250
            },
            {
              name: '2020/03/05',
              value: 300
            },
            {
              name: '2020/03/06',
              value: 450
            },
            {
              name: '2020/03/07',
              value: 500
            },
            {
              name: '2020/03/08',
              value: 700
            }
          ]
        }
      ];

    constructor(httpService: HttpService) {
      super(httpService);
      this.baseUrl = EndpointsConstants.dashboard.base;
    }

    getDashboardMain(filters: any = []): Observable<BaseResponse<DashboardMain>> {
      const url = this.baseUrl + EndpointsConstants.dashboard.main;
      return this.httpService.get<DashboardMain>(url, filters, true);
    }

  getGraph(filters): Observable<any> {
    const url = this.baseUrl + EndpointsConstants.dashboard.graph;
    return this.httpService.get<any>(url, filters,  true);
  }

}
