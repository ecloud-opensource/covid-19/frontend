import { Injectable } from '@angular/core';
import { HttpService } from '../http/http.service';
import { EndpointsConstants } from 'src/app/helper/endpoints';
import { ApiService } from '../api/api.service';
import { BaseResponse } from 'src/app/models/base-response';
import { Observable } from 'rxjs';
import { Log } from 'src/app/models/log';
import { generateParamsForRoutes } from 'src/app/helper/utils';

@Injectable({
  providedIn: 'root'
})
export class CountryService extends ApiService {

  constructor(httpService: HttpService) {
    super(httpService);
    this.baseUrl = EndpointsConstants.country.base;
  }

  log(filters: any = []): Observable<BaseResponse<Log>> {
    const url = this.baseUrl + EndpointsConstants.country.log;
    const queryParams = generateParamsForRoutes(filters);
    return this.httpService.get<Log>(url, queryParams, true);
  }

}
