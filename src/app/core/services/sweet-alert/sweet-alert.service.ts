import { Injectable } from '@angular/core';
import swal, { SweetAlertIcon } from 'sweetalert2';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
    providedIn: 'root'
})
export class SweetAlertService {

    constructor(private translateService: TranslateService) { }

    confirmAction(title?, text?) {
        return swal.fire({
            title: title ? this.translateService.instant(title) : '',
            text: text ? this.translateService.instant(text) : '',
            showCancelButton: true,
            confirmButtonColor: '#0CC27E',
            cancelButtonColor: '#FF0000',
            confirmButtonText: this.translateService.instant('forms.buttons.yes'),
            cancelButtonText: this.translateService.instant('forms.buttons.cancel'),
            reverseButtons: true
        });
    }

    success(title?, text?, icon: SweetAlertIcon = 'success') {
        return swal.fire({
            title: title ? this.translateService.instant(title) : '',
            text: text ? this.translateService.instant(text) : '',
            icon
        });
    }
}
