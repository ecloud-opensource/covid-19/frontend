import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpService } from '../http/http.service';
import { EndpointsConstants } from 'src/app/helper/endpoints';
import { setLocalData, removeLocalData, getLocalData } from 'src/app/helper/storage';
import { Router } from '@angular/router';
import { ToastService } from '../toast/toast.service';
import { isNull } from 'util';
import { Admin } from 'src/app/models/admin';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private router: Router,
    private httpService: HttpService,
    private toast: ToastService
  ) { }

  login(email: string, password: string): Observable<boolean> {
    const url = EndpointsConstants.admin.base + EndpointsConstants.admin.login;

    return new Observable<boolean>(observer => {
      this.httpService.post(url, { email, password }, true).subscribe(
        response => {
          this.setLoginData(response);
          observer.next(true);
        });
    });
  }

  setLoginData(response = null) {
    const token = response.data.token;
    const admin = new Admin(response.data.admin);

    this.setCurrentAdmin(admin);
    setLocalData('covid19-admin-token', token);

    this.toast.success('', 'services.auth.welcome');

    this.router.navigate(['/dashboard']);
  }

  logout(): void {
    removeLocalData('covid19-admin-currentAdmin');
    removeLocalData('covid19-admin-token');
    this.router.navigate(['/auth', 'login']);
  }

  isAuthenticated(redirect: boolean = true): boolean {
    const currentAdmin = getLocalData('covid19-admin-currentAdmin');
    if (!currentAdmin) {
      if (redirect) {
        this.router.navigate(['/auth', 'login']);
      }
      return false;
    }
    return true;
  }

  forgotPassword(email: string) {
    const url = EndpointsConstants.admin.base + EndpointsConstants.admin.forgotPassword;
    return this.httpService.put(url, { email });
  }

  restorePassword(password: string, token: string) {
    const url = EndpointsConstants.admin.base + EndpointsConstants.admin.resetPassword;
    const params: any = {
      newPassword: password,
      token
    };
    return this.httpService.put(url, params);
  }

  setCurrentAdmin(admin: Admin) {
    setLocalData('covid19-admin-currentAdmin', new Admin(admin));
  }

  getCurrentAdmin() {
    const admin = getLocalData('covid19-admin-currentAdmin');
    return !isNull(admin) ? new Admin(admin) : null;
  }
}
