import { Injectable } from '@angular/core';
import { HttpService } from '../http/http.service';
import { EndpointsConstants } from 'src/app/helper/endpoints';
import { ApiService } from '../api/api.service';
import { Observable } from 'rxjs';
import { BaseResponse } from 'src/app/models/base-response';
import { generateParamsForRoutes } from 'src/app/helper/utils';
import { Log } from 'src/app/models/log';
import { Ranking } from 'src/app/models/ranking';

@Injectable({
  providedIn: 'root'
})
export class CityService extends ApiService {

  constructor(httpService: HttpService) {
    super(httpService);
    this.baseUrl = EndpointsConstants.city.base;
  }

  log(filters: any = []): Observable<BaseResponse<Log>> {
    const url = this.baseUrl + EndpointsConstants.city.log;
    const queryParams = generateParamsForRoutes(filters);
    return this.httpService.get<Log>(url, queryParams, true);
  }

  ranking(filters: any = []): Observable<BaseResponse<Ranking>> {
    const url = this.baseUrl + EndpointsConstants.city.ranking;
    const queryParams = generateParamsForRoutes(filters);
    return this.httpService.get<Ranking>(url, queryParams, true);
  }

}
