import { Injectable } from '@angular/core';
import { ApiService } from '../api/api.service';
import { BaseResponse } from 'src/app/models/base-response';
import { Observable } from 'rxjs';
import { BaseEntity } from 'src/app/models/base-entity';
import { EndpointsConstants } from 'src/app/helper/endpoints';
import { HttpService } from '../http/http.service';

@Injectable({
  providedIn: 'root'
})
export class GeoService extends ApiService {

  constructor(httpService: HttpService) {
    super(httpService);
    this.baseUrl = EndpointsConstants.geo.base;
  }

  getAdminNearBy<T extends BaseEntity<T>>(params): Observable<BaseResponse<T>> {
    const url = this.baseUrl + EndpointsConstants.geo.adminNearBy;
    return this.httpService.post(url, params, false);
  }
}
